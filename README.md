Sujet et commande

Création d’éléments de communication pour le site d’Elbeuf, DUT et LPMN.

Avancer le côté artistique de la formation, sans laisser de côté la technicité : traduire l’idée que les technologies numériques permettent une expression artistique.



Identité graphique globale

Développer un panel de références afin d’établir une charte graphique, applicable au reste des productions.


Production photographique

Images orientées photographie subjective et artistique.
Jeu typographique, intégrations diverses (affiches, usage sur réseaux sociaux, sur site web, etc).

Photographie subjective :  qui ne donne pas une représentation fidèle de la chose observée, réinterprétation du réel. A contre-courant de la standardisation des images.

Idée de l’instant T venant de la photo de rue, photo de reportage…


Production audiovisuelle

Trailer de  ~ 30 sec pour la JPO avec éléments de Motion Design réutilisables (logo animé, identité motion, etc).

Eléments pour campagnes virales soit gif par exemple.


Développement Web

Page de site à revoir, avec maquettage, wireframe puis peut-être production finale.


Environnement graphique tridimensionnel

Apporter l’identité graphique au site d’Elbeuf, sous forme de bâches, kakémonos (utilisation portes ouvertes/forum dans lycées, etc).


