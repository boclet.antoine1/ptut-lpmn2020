var lottieContainerMain = document.getElementById('lottieContainerMain');
var animData = {
    container: lottieContainerMain,
    renderer: 'svg',
    loop: true,
    prerender: false,
    autoplay: true,
    autoloadSegments: false,
    path: 'motions/principal.json'
};

var animMain;
animMain = bodymovin.loadAnimation(animData);

// lottieContainerMain.addEventListener("mouseenter", function() {
//     animMain.loop = true;
//     animMain.play();
// });
// lottieContainerMain.addEventListener("mouseleave", function() {
//     animMain.loop = false;
// });