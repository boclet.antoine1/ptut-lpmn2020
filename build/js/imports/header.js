window.onscroll = function(){
	if(screen.width > 992){
		var header = document.getElementsByTagName('header')[0];
	    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
	   		header.style.height ='10vh';
	    } 
	  	else {
	   		header.style.height = '20vh';
  		}
  	}
};


(function($){
   
var state = 0,
    vw = $(window).width();
$("#hamburger").click(function() {
  var delay_time = 0;
  $(this).toggleClass('open');
  console.log(state);
  if (state === 0) {
    TweenMax.to($("#bg-menu-mobile"), 1, {
      x:-vw,
      ease: Expo.easeInOut
    });
    
    $("header li").each(function() {
      TweenMax.to($(this), 1.2, {
        x:-vw,
        scaleX: 1,
        delay: delay_time,
        ease: Expo.easeInOut
      });
      delay_time += .04;
      $("header ul").css("display", "block");
    });
    state = 1;
  } else {
    state = 0;
    TweenMax.to($("#bg-menu-mobile"), 1.2, {
      x:0,
      ease: Expo.easeInOut
    });
    $(" header li").each(function() {
      TweenMax.to($(this), 1, {
        x:0,
        delay: delay_time,
        ease: Expo.easeInOut
      });
      delay_time += .02;
      setTimeout(function() {
        $("header ul").css("display", "none");
      }, 1000);
    });
  }	
});
})(jQuery);
