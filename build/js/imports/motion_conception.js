var lottieContainerConception = document.getElementById('lottieContainerConception');
var animData = {
    container: lottieContainerConception,
    renderer: 'svg',
    loop: false,
    prerender: false,
    autoplay: true,
    autoloadSegments: false,
    path: 'motions/conception.json'
};

var animConception;
animConception = bodymovin.loadAnimation(animData);

lottieContainerConception.addEventListener("mouseenter", function() {
    animConception.loop = true;
    animConception.play();
});
lottieContainerConception.addEventListener("mouseleave", function() {
    animConception.loop = false;
});