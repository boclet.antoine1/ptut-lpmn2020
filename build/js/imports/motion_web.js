var lottieContainerWeb = document.getElementById('lottieContainerWeb');
var animData = {
    container: lottieContainerWeb,
    renderer: 'svg',
    loop: false,
    prerender: false,
    autoplay: true,
    autoloadSegments: false,
    path: 'motions/web.json'
};

var animWeb;
animWeb = bodymovin.loadAnimation(animData);

lottieContainerWeb.addEventListener("mouseenter", function() {
    animWeb.loop = true;
    animWeb.play();
});
lottieContainerWeb.addEventListener("mouseleave", function() {
    animWeb.loop = false;
});