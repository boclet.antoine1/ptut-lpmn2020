var lottieContainerCom = document.getElementById('lottieContainerCom');
var animData = {
    container: lottieContainerCom,
    renderer: 'svg',
    loop: false,
    prerender: false,
    autoplay: true,
    autoloadSegments: false,
    path: 'motions/comm.json'
};

var animCom;
animCom = bodymovin.loadAnimation(animData);

lottieContainerCom.addEventListener("mouseenter", function() {
    animCom.loop = true;
    animCom.play();
});
lottieContainerCom.addEventListener("mouseleave", function() {
    animCom.loop = false;
});