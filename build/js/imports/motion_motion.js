var lottieContainerMotion = document.getElementById('lottieContainerMotion');
var animData = {
    container: lottieContainerMotion,
    renderer: 'svg',
    loop: false,
    prerender: false,
    autoplay: true,
    autoloadSegments: false,
    path: 'motions/motion.json'
};

var animMotion;
animMotion = bodymovin.loadAnimation(animData);

lottieContainerMotion.addEventListener("mouseenter", function() {
    animMotion.loop = true;
    animMotion.play();
});
lottieContainerMotion.addEventListener("mouseleave", function() {
    animMotion.loop = false;
});