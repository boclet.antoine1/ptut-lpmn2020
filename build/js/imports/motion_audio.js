var lottieContainerAudio = document.getElementById('lottieContainerAudio');
var animData = {
    container: lottieContainerAudio,
    renderer: 'svg',
    loop: false,
    prerender: false,
    autoplay: true,
    autoloadSegments: false,
    path: 'motions/audio.json'
};

var animAudio;
animAudio = bodymovin.loadAnimation(animData);

lottieContainerAudio.addEventListener("mouseenter", function() {
    animAudio.loop = true;
    animAudio.play();
});
lottieContainerAudio.addEventListener("mouseleave", function() {
    animAudio.loop = false;
});