var lottieContainerGraphisme = document.getElementById('lottieContainerGraphisme');
var animData = {
    container: lottieContainerGraphisme,
    renderer: 'svg',
    loop: false,
    prerender: false,
    autoplay: true,
    autoloadSegments: false,
    path: 'motions/graphisme.json'
};

var animGraphisme;
animGraphisme = bodymovin.loadAnimation(animData);

lottieContainerGraphisme.addEventListener("mouseenter", function() {
    animGraphisme.loop = true;
    animGraphisme.play();
});
lottieContainerGraphisme.addEventListener("mouseleave", function() {
    animGraphisme.loop = false;
});