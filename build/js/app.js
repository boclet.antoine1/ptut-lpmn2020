//@prepros-prepend imports/header.js
//@prepros-prepend imports/jquery.instagramFeed.js
//@prepros-prepend imports/aos.js

// @prepros-append imports/motion_principale.js
// @prepros-append imports/motion_web.js
// @prepros-append imports/motion_com.js
// @prepros-append imports/motion_graphisme.js
// @prepros-append imports/motion_conception.js
// @prepros-append imports/motion_audio.js
// @prepros-append imports/motion_motion.js


AOS.init();

$(function(){
  $('.accordeon_content').hide();
  
  $('.accordeon_title').click(function(){
    $(this).parent().toggleClass('active').siblings().removeClass('active');
    $('.accordeon_content').slideUp();
    
    if(!$(this).next().is(":visible")) {
			$(this).next().slideDown();
		}
  });
});

$('.voirPlus').on('click', function() {
  	if ($('.mat>div').hasClass("cache")){
  		$('.mat>.cache').addClass('show');
  		$('.mat>div').removeClass('cache');
  	}
  	else if ($('.mat>div').hasClass("show")){
  		$('.mat>.show').addClass('cache');
  		$('.mat>div').removeClass('show');	
  	}
  	else if ($('.profs>div').hasClass("show")){
  		$('.profs>.show').addClass('cache');
  		$('.profs>div').removeClass('show');
  	}
  	else if ($('.profs>div').hasClass("cache")){
  		$('.profs>.cache').addClass('show');
  		$('.profs>div').removeClass('cache');
  	}
});

window.onload = function(){
  var playerControls = document.getElementsByClassName('player-controls');
  //alert(playerControls);
  playerControls.style.display = 'none';
};