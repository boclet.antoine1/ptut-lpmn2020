<?php $current = "Accueil";
$title = "Accueil | DUT MMI | IUT de Rouen, Elbeuf";
$description = "Site Internet du département MMI de l'IUT d'Elbeuf. Site réalisé dans le cadre des projets tuteurés, agence Colab 2017-2018. Ce DUT MMI vous permettra de poursuivre sur le site d'Elbeuf en licence professionnelle métiers du numérique conception réalisation rédaction WEB : LP MN CRR-WEB";

include_once('_inc/header.php'); ?>            
<div class="accueil">
    <article class="enteteBoite catchline">
         <div id="lottieContainerMain"></div>
        <h1 id="phraseMMI">"La seule formation publique post-bac dans le domaine de <span>la communication numérique</span> et de <span>l'internet</span>."</h1>
        <a href="https://iutmmi.fr"><p id="iutmmi">iutmmi.fr</p></a>
        <div id="triangle"></div>
        <a href="#top"><span class="dot"><img src="img/icons/down-arrow.svg"></span></a>

    </article>

    <a class="anchor" id="top"></a>
    <section class="description_formation">
        <article>
        <p class="texte">
            Notre objectif est de former des professionnels du web et du multimédia capables de travailler dans divers domaines.
        </p>
        <p class="texte">
            Pluridisciplinaire, le DUT MMI couvre tous les champs du numérique, du design graphique au développement de site web, de la création vidéo à la conception de contenus.
        </p>
        <button class="buttonBleu" onclick="location.href='formation.php';" value="Formation">Découvrir la formation</button>
        </article>
    </section>
    
<div class="fondBleu">
    
    <article class="accueilmat" data-aos="fade-up" data-aos-duration="750">
        <div>
            <div id="lottieContainerWeb"></div>
            <p class="NomMat">Développement Web</p>
        </div>
        <div>
            <div id="lottieContainerCom"></div>
            <p class="NomMat">Communication</p>
        </div>
        <div>
            <div id="lottieContainerGraphisme"></div>
            <p class="NomMat">Graphisme</p>
        </div>
        <div>
            <div id="lottieContainerConception"></div>
            <p class="NomMat">Conception artistique</p>
        </div>

        <a href="formation.php"><div id="lienmat">+</div></a>
    </article>

    

    </div>
    <article class="l-IUT">
        <p class="texte texteMobile">L'IUT d'Elbeuf est construit sur le site des anciennes industries textiles. Il a été totalement réhabilité en 2002 afin d'y construire un nouveau bâtiment.</p>
        <div id="imgIUT"></div>
        <div>
            <p class="texte textePC">L'IUT d'Elbeuf est construit sur le site des anciennes industries textiles. Il a été totalement réhabilité en 2002 afin d'y construire un nouveau bâtiment.</p>
            <p class="texte deuxieme">Cette antenne de l'IUT de Rouen comprend un amphi de plus de 120 places, de nombreuses salles de cours, salles de Travaux Pratiques dédiées à la programmation et à l'intégration web ou bien à l'infographie, ainsi qu'un studio adapté à l'enseignement de la création vidéo.</p>
            <div>
                <button class="buttonBleu" onclick="location.href='departement.php';" value="Département">Découvrir les locaux </button>
            </div>
        </div>
    </article>
    <div class="numero num">
            <div>
                <div>4</div>
                <div>Semestres</div>
            </div>
            <div>
                <div>10</div>
                <div>Semaines de stage</div>
            </div>
            <div>
                <div>78</div>
                <div>Places</div>
            </div>
            <div>
                <div>1</div>
                <div>Table de ping pong</div>
            </div>
        </div>

</div>
<?php include_once('_inc/footer.php') ?>
