$(function(){
$(window).load(function(){

	/* MENU PORTABLE */
	$('#principal_menu_button').click(function(){ // si on clique sur le boutton
		if ($('#principal_menu_list').css('display') == "none"){ // si la liste n'est pas affichée
			$('#principal_menu_button').attr('class', 'fa fa-times');
			$('#principal_menu_list').slideDown();
		}
		else{ // si la liste est affichée
			$('#principal_menu_button').attr('class', 'fa fa-bars');
			$('#principal_menu_list').slideUp();
		}
	});

});
});
