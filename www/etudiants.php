<?php $current = "Étudiants";
$title = "Étudiants | DUT MMI | IUT de Rouen, Elbeuf";
$description = "Site Internet du département MMI de l'IUT d'Elbeuf. Site réalisé dans le cadre des projets tuteurés, agence Colab 2017-2018. Ce DUT MMI vous permettra de poursuivre sur le site d'Elbeuf en licence professionnelle métiers du numérique conception réalisation rédaction WEB : LP MN CRR-WEB";

	include_once('_inc/header.php'); ?>

	<div class="flexContainer enteteBoite" id="etudiants">
		<div>
			<div class="titreBleu">Les étudiants</div>
			<div class="titre">Leurs projets</div>
				<p class="entete">La pédagogie par projet permet de mettre en pratique les cours théoriques dans le but d'une meilleure compréhension.
				<br/><br/>
				Les projets tutorés, comme la création d'une agence fictive, permettent de tisser des liens entre toutes les matières et d'améliorer la cohésion d'équipe.</p>
		</div>
		<div class="boxImg">
		<img src="img/etudiants.jpg" alt="image d étudiant au travail" class="etudiant">
		</div>
	</div>
	<div class="flexButton">
		<button class="buttonBleu" value="Portfolios"><a href="portfolios/index.html">Portfolios</a></button>
		<button class="buttonBleu" value="ProjetsPTUT"><a href="projets.php">Projets tutorés</a></button>
		<button class="buttonBleu" value="Projets"><a href="galerie2016/index.php">Galerie de projets</a></button>
	</div>
	<div id="instagram-feed-demo" class="instagram_feed"></div>


	<div class="numero saut">
			<div>
				<div>87%</div>
				<div>de réussite</div>
			</div>
			<div>
				<div>90%</div>
				<div>de poursuite d'études</div>
			</div>
			<div>
				<div>40%</div>
				<div>de Bac S</div>
			</div>
			<div>
				<div>30%</div>
				<div>de Bac Techno</div>
			</div>
		</div>	
		<div class="fondBleu">
			<article class="poursuite">
				<p>Le DUT MMI est une formation pluridisciplinaire qui ouvre les champs à de multiples poursuites d’études.
				<br/><br/>
				Ecoles du web, écoles d’audiovisuels, écoles d’ingénieurs, mais aussi des licences professionnelles, notamment la Licence Professionnelle Métiers du Numérique Conception Rédaction et Réalisation Web à Elbeuf.</p>
				<button class="buttonBlanc" onclick="location.href='https://mmirouen.fr/lpmn/index.php';" value="Licence Pro Métiers du Numérique"> Découvrir la licence</button>
			</article>
		</div>
		<article class="flexContainer inter">
			<div>
				<div class="titre2">L'international</div>
				<p>Vous aurez la possibilité de faire votre dernier semestre et votre stage au CEGEP de MATANE.</p>
				<br/>
				<p>Sur place vous réaliserez un projet innovant en groupe avec les étudiant québécois : application mobile, site internet ou jeu vidéo.</p>
			</div>
			<img src="img/map.svg" alt="" class="map">
		</article>
		<div class="fondBleu">
			<article id="temoignages">
				<h2>Témoignage</h2>
				<video width="832" height="468" controls><source src="https://webtv.univ-rouen.fr/downloads/file/v125f358491663r7svh6/?url=https%3A//webtv.univ-rouen.fr/resources/r125f35849166x0d0l7evgk6aa132y/v125f358491663r7svh6_720.mp4" type="video/mp4"></video>
		    </article>
		</div>






<?php include_once('_inc/footer.php') ?>