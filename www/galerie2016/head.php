<!DOCTYPE html>
<html>
	<head>
		<!-- LANGUE -->
		<html lang="fr">

		<!-- INFOS -->
		<title>IUT MMI Elbeuf | Galerie</title>
		<meta name="keywords" content="MMI, galerie, réalistions, étudiants, élève, audiovisuel, infographie, programmation">
		<meta name="description" content="Galerie des réalisations des étudiants de l'IUT MMI d'Elbeuf">
		<meta name="author" content="Valink">

		<!-- FAVICON -->

		<link rel="apple-touch-icon" sizes="180x180" href="img/favicon_package/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="img/favicon_package/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="img/favicon_package/favicon-16x16.png">
		<link rel="manifest" href="img/favicon_package/site.webmanifest">
		<link rel="mask-icon" href="img/favicon_package/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">

		<!-- ENCODEAGE -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<!-- LIBRAIRIES -->
		<link rel="stylesheet" type="text/css" href="font/font-awesome-4.4.0/css/font-awesome.min.css">
		<script type="text/javascript" src="js/jquery.js"></script>

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/header_commun_footer.css">

		<!-- JS -->
		<script type="text/javascript" src="js/header_commun_footer.js"></script>

		<!-- RESPONSIVE -->
		<meta name="viewport" content="width=device-width" />

	<!-- </head> fermeture dans le contenu de la page -->
