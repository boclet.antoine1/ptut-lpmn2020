<?php
// CONNEXION À LA BASE DE DONNÉE
require('admin/connect.php');

// CLASSES ET GESTIONNAIRE
function chargerClasse($classe){
	require 'class/'.$classe.'.php';
}
spl_autoload_register('chargerClasse');

$RealisationManager = new RealisationManager($db);

require('admin/affichages.php');
?>

<?php include 'head.php'; ?>
	<link rel="stylesheet" type="text/css" href="css/galerie.css" />
	<script src="js/galerie.js"></script>
</head>
<body>
<header>

	<h1>
		<a href="index.php">
			<img src="img/header/logo.png" alt="logo MMI" />
			<p>LES RÉALISATIONS</p>
		</a>
	</h1>

	<nav>
		<ul id='tous' >
			<?php
				$get_domaine = get_domaine($db);
				while ($donnees_domaine = $get_domaine->fetch(PDO::FETCH_ASSOC)){
			?>
				<li>
					<p class="tri_domaine" id="<?= $donnees_domaine['domaine']; ?>"><?= $donnees_domaine['domaine']; ?></p>
					<!-- <ul class="tri_liste" id="type"> -->
					<ul id="type">
						<?php
							$get_type_domaine = get_type_domaine($db, $donnees_domaine['domaine']);
							while ($donnees_type_domaine = $get_type_domaine->fetch(PDO::FETCH_ASSOC)){
						?>
							<li><p class="tri_item"><?= $donnees_type_domaine['type']; ?></p></li>
						<?php
							}
							$get_type_domaine->closeCursor();
						?>
					</ul>
				</li>
				<span> | </span>
			<?php
				}
				$get_domaine->closeCursor();
			?>
			<li>
				<p>Promotions</p>
				<ul id="promotion">
					<?php
						$get_type_promotion = get_promotion($db);
						while ($donnees_type_promotion = $get_type_promotion->fetch(PDO::FETCH_ASSOC)){
					?>
						<li><p class="tri_item"><?= $donnees_type_promotion['promotion']; ?></p></li>
					<?php
						}
						$get_type_domaine->closeCursor();
					?>
				</ul>
			</li>
			<span> | </span>
			<li><p class="tri_item">TOUTES</p></li>
		</ul>
		<p id="fil">&nbsp;</p>
	</nav>
</header>

<section id="galerie" class="corps">

	<div id="catalogue">
		<p id="catalogue_infos">Aucune realisation avec ce critère pour le moment</p>
		<?php
			$realisation_list = $RealisationManager->getList();
			foreach ($realisation_list as $key => $value) {
		?>
				<form method="POST" class="realisation" action="<?= "details.php"."?".$realisation_list[$key]->realisation(); ?>">
				  <input type="hidden" name="id" value="<?= $realisation_list[$key]->id() ?>" />
					<?php
						switch ($realisation_list[$key]->type()) {
							case 'animations':
							case 'films':
							case 'photos':
								$domaine = 'audiovisuel';
								break;
							case '3D':
							case 'affiches':
							case 'carte de visite':
							case 'identité visuelle':
							case 'logos':
							case 'maquette':
								$domaine = 'infographie';
								break;
							case 'applications':
							case 'jeux':
							case 'sites':
								$domaine = 'programmation';
								break;
						}
					?>
				  <input type="hidden" name="domaine" value="<?= $domaine ?>" />
				  <input type="hidden" name="type" value="<?= $realisation_list[$key]->type() ?>" />
				  <input type="hidden" name="promotion" value="<?= $realisation_list[$key]->promotion() ?>" />

				  <button>
				    <?php if (is_file("img/realisation/".$realisation_list[$key]->img_principale())){ ?>
				      <img src="<?= "img/realisation/miniature/".$realisation_list[$key]->img_principale(); ?>" alt="<?= $realisation_list[$key]->realisation(); ?>"/>
				    <?php }
				    else{ ?>
				      <img src="img/realisation/miniature/defaut.jpg" alt="logo francky la chocolaterie"/>
				    <?php } ?>

				    <div class='infos'>
							<?php if ($realisation_list[$key]->realisation()): ?>
					      <h3 class="nom" value="<?= $realisation_list[$key]->realisation() ?>">
					        <?= $realisation_list[$key]->realisation() ?>
					      </h3>
								<br/>
								<hr/>
								<br/>
							<?php endif; ?>
							<?php if ($realisation_list[$key]->description()): ?>
					      <p>
									<?= substr ( $realisation_list[$key]->description() , 0 , 75 ) ?>...
					      </p>
							<?php endif; ?>
				    </div>
				  </button>
				</form>
		<?php
			}
		?>

	</div>

</section>

<?php require('footer.php'); ?>
