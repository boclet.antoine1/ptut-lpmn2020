-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 24 Mars 2016 à 06:13
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `valink`
--

-- --------------------------------------------------------

--
-- Structure de la table `mmi_promotion`
--

CREATE TABLE IF NOT EXISTS `mmi_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `mmi_promotion`
--

INSERT INTO `mmi_promotion` (`id`, `promotion`) VALUES
(4, '2014 - 2015'),
(8, 'Antérieure à 2014'),
(10, '2015 - 2016');

-- --------------------------------------------------------

--
-- Structure de la table `mmi_realisation`
--

CREATE TABLE IF NOT EXISTS `mmi_realisation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `realisation` varchar(64) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `type` varchar(128) DEFAULT NULL,
  `code` varchar(1024) NOT NULL,
  `img_principale` varchar(64) DEFAULT NULL,
  `img_secondaire` varchar(512) DEFAULT NULL,
  `promotion` varchar(32) DEFAULT NULL,
  `auteur` varchar(128) DEFAULT NULL,
  `lien` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=197 ;

--
-- Contenu de la table `mmi_realisation`
--

INSERT INTO `mmi_realisation` (`id`, `realisation`, `description`, `type`, `code`, `img_principale`, `img_secondaire`, `promotion`, `auteur`, `lien`) VALUES
(143, 'Affiche photographiques', 'Les étudiants de première année ont exploité des fichiers photographiques obtenus lors d’un TP précédent (consacré à la prise de vue photo et à ses principaux paramètres) pour produire une affiche qui annonce une exposition de design graphique à Elbeuf . La consigne était de créer un dialogue entre l’espace de l’image et celui du texte au service d’une ergonomie de l’information.', 'affiches', '', 'Chloe Argentin.jpg', 'Alexandre Briere.jpg, Corentin Monmasson.jpg, Jimmy Beaudoin.jpg, Loic Catrou.jpg, Louise Lelievre.jpg, Nicolas Fortin.jpg, Pauline Duval.jpg, Yassine Houays.jpg', '2015 - 2016', '', ''),
(151, 'Film présentation agence', 'Film d''agence de la Cerise Créative version courte réalisée en première année de DUT MMI.', 'animations', '<iframe src="https://www.youtube.com/embed/Lb60UxX2NYI" frameborder="0" allowfullscreen></iframe>', '841227.jpg', '', '2015 - 2016', 'La Cerise Créative', 'http://lacerisecreative.valink.fr/'),
(152, 'Un matin comme les autres', 'Production audiovisuelle réalisée en TP - exercice du crescendo', 'films', '<iframe src="https://www.youtube.com/embed/0-Wxd03-GNQ" frameborder="0" allowfullscreen></iframe>', '5613560.jpg', '', '2015 - 2016', 'La Cerise Créative', 'http://lacerisecreative.valink.fr/'),
(153, 'Site agence', 'Site d''une agence de communication fictive, réalisée dans le cadre des projets tutorés', 'sites', '<iframe width=800 height=600 src="http://lacerisecreative.valink.fr/" width=600 height=200 scrolling=auto frameborder=1 > </iframe>', '56135836.jpg', '', '2015 - 2016', 'Valentint Sourice', 'http://valink.fr/'),
(154, 'Trailer JPO 2015', 'Animation réalisée dans le cadre de la journée porte ouvert', 'animations', '<iframe width="560" height="315" src="https://www.youtube.com/embed/tHqlSVlKU_k" frameborder="0" allowfullscreen></iframe>', '56141142.jpg', '', '2015 - 2016', 'La Cerise Créative', 'http://lacerisecreative.valink.fr/'),
(155, 'Affiche haiga', 'Affiche pour une exposition d''Haiga réalisée pendant la JPO 2015', 'affiches', '', 'indra simon.jpg', '', '2014 - 2015', 'Indra Simon', 'http://indrasimon.valink.fr/'),
(158, 'Maquette site agence', '', 'maquette', '<iframe src="test.pdf" align="middle"></iframe>', '56153512.jpg', '', '2014 - 2015', 'Indra Simon', 'http://indrasimon.valink.fr/'),
(160, 'Carte de visite agence', '', 'carte de visite', '', '2.jpg', '1.jpg', '2014 - 2015', 'Indra Simon', 'http://indrasimon.valink.fr/'),
(161, 'Photo sujet & architecture', 'Avec comme thème imposé "Sujet & Architecture", des étudiants de première année avait pour consigne d''être le plus créatif possible, en jouant sur les ombres, les lumières, les perspectives, les reflets...<br />\r\nIl s''agissait avant tout d''un projet d''expérimentation afin de développer la créativité des étudiants.<br />\r\n<br />\r\nAlexia Léger, Elodie Amorim, Lucie Treussart, ME Michel, Marine Hauduc, Marine Delaye, Romane Bazemont', 'photos', '', 'ALeger.jpeg', 'EAmorim.jpeg, LTreussart.jpeg, MDelaye.jpeg, MEMichel.jpeg, MHauduc.jpeg, RBazemont.jpeg', 'Antérieure à 2014', '', ''),
(162, 'Photographie en clair obscur', 'Afin de prendre en main les appareils photo 5D de Sony, les étudiants MMI1 de 2013-2014 ont expérimenté sur le clair-obscur. L''emplacement des lumières et l''ouverture du diaphragme de l''appareil étaient également des points importants à prendre en compte.<br />\r\n<br />\r\nLéo Fabre (modèles: Georges Graire et Maryne Hauduc), Pascaline Petit (modèle: Charlotte Olivier), Willy Triffault (modèle: Thibault Saussey)', 'photos', '', 'WTriffault.jpeg', 'PPetit.jpeg, LFabre.jpeg', 'Antérieure à 2014', '', ''),
(163, 'Affiches pour le Cirque Théâtre d''Elbeuf', 'Mme Bredel - Infographie 2D - MMI1 2013-2014<br />\r\n<br />\r\nDans le cadre des cours d''infographie, des étudiants devaient réaliser une campagne print pour le lancement de la saison 2013-2014 du CTE. Le texte était imposé, le logo devait être redessiné, et la composition devait se baser sur le principe du photomontage.<br />\r\nLe visuel devait retranscrire une ou plusieurs des approches suivantes : équilibre/déséquilibre, rêve, imagination, irréel, aérien, animal...<br />\r\n<br />\r\nAntoine Bouraly, Charlotte O', 'affiches', '', 'SElAbbadi.jpg', 'ABouraly.jpg, COlivier.jpg, EZaccaro.JPG, GCassin.png, MCBana.JPG, MPesquet.JPG, MPhilippot.jpg, RMartinho.JPG, TSchmitt.jpg', 'Antérieure à 2014', '', ''),
(164, 'Sites d''agences de communication fictives', 'Pour la promotion 2013-2014, les projet tuteurés de première année consistait en la réalisation d''agences de communications fictives, afin de pouvoir en deuxième année géré un véritable projet dans sa totalité.<br />\r\nCubunity, Digital Dream, Dzing Studio, eMotion Time, Lumea, Mate, Pimp It, Vizi On<br />\r\nCliquez sur la grande image pour accéder au site internet de l''agence !', 'logos', '', 'emotiontime.jpg', 'cubunity.jpg, digitaldream.jpg, dzingstudio.jpg, lumea.jpg, mate.png, pimpit.png, vizion.png', 'Antérieure à 2014', '', ''),
(165, 'Journée Portes Ouvertes 2015', '<br />\r\n<br />\r\nLe groupe de projet tuteuré de MMI2 de la promotion 2014-15 nommé eMotion Time a eu, entre autre, pour tâche de réaliser la communication autour des JPO de l''établissement. Ils ont donc travaillé sur plusieurs affiches et flyers dont voici la version finale, affichées dans tous les lycées de l''agglomération de Rouen pour l''occasion.<br />\r\n<br />\r\nMarie-Eléonore Michel, Florian Pommier, Marion Prigent, Lucie Treussart.', 'affiches', '', 'JPO.jpg', '', 'Antérieure à 2014', '', ''),
(166, 'Journée des Métiers 2015', '<br />\r\n<br />\r\nLa Journée des Métiers est un événements annuel de l''IUT, permettant aux élèves de rencontrer des professionnels du graphisme, du web, de la vidéo et de la communication en général.<br />\r\nEn 2015, cette JdM organisée par l''agence Pimp It regroupant des étudiants de MMI2, a également donné lieu à la création de flyers, d''affiches, de vidéos et d''un site internet.<br />\r\n<br />\r\nElodie Amorim, Romane Bazemont, Léa Bouissiere, Sarah El Abaddi, Enzo Zaccaro.', 'affiches', '', 'JDM.JPG', '', 'Antérieure à 2014', '', ''),
(167, 'Les 24H de l''Audiovisuel', 'Les 24h de l''Audiovisuel est un des événements spécifiques à la formation MMI de l''IUT d''Elbeuf. Tous les deux ans, les deux promotions se réunissent en groupe d''environ une dizaine d''étudiants pour imaginer et réaliser un court-métrage complet en moins de 24h. En 2014, huit équipes étaient en compétition, aidé par des professionnels de l''audiovisuel ainsi que plusieurs anciens élèves, revenus pour l''occasion.', 'films', '', 'ToysofWonder.jpg', 'GameLimit.png, Amy.png, Brainstorming.png, Decadence.png, LAffaire404.png, PlayAgain.png, ToysofWonder.jpg, Unchained.png', 'Antérieure à 2014', '', ''),
(168, 'Affiche Portes ouvertes', 'Portes ouvertes au département MMI IUT Elbeuf,<br />\r\nle 07 février 2015, Nous vous attendons ce samedi !', 'photos', '', 'rdhdjtfjftxjtfxjt (4).jpg', 'rdhdjtfjftxjtfxjt (3).jpg, rdhdjtfjftxjtfxjt (5).JPG, rdhdjtfjftxjtfxjt (7).jpg', 'Antérieure à 2014', '', ''),
(169, 'TEASER @MOTION', 'Réalisé pour la première WebTV du projet tuteuré PlayMotion. Il vous donnera un avant-goût des productions SRC présentées dans cette galerie.', 'films', '<video controls src="video/video_webtv1_amotion.mp4">video_webtv1_amotion.mp4</video>', 'video_webtv1_amotion_img.png', '', 'Antérieure à 2014', '', ''),
(170, 'SRC FACTORY', 'Trailer du groupe SRC Factory du projet tuteuré SRC Département de la promotion SRC2 2011-2012 pour la Journée Portes Ouvertes 2012.', 'animations', '<video controls src="video/video_src_factory_jpo.mp4">video_src_factory_jpo.mp4</video>', 'video_src_factory_jpo_img.png', '', 'Antérieure à 2014', '', ''),
(171, 'LIPDUB DE L''IUT', 'Réalisé grâce à 140 participants, étudiants en SRC de 2008, ce lipdub de l''IUT d''Elbeuf a été vu des millers de fois à travers le monde.', 'films', '<video controls src="video/lipdub.mp4">lipdub.mp4</video>', 'video_lipdub_iut_img.png', '', 'Antérieure à 2014', '', ''),
(172, 'CRESCENDO', 'Court-métrage réalisé dans le cadre d''un projet audiovisuel par le TP1 de la promotion SRC1 2010-2011.', 'films', '<video controls src="video/video_cm_crescendo.mp4">video_cm_crescendo.mp4</video>', 'video_cm_crescendo_img.png', '', 'Antérieure à 2014', '', ''),
(173, 'ANDREW JINN', 'Court-métrage réalisé dans le cadre d''un projet audiovisuel par le TP5 de la promotion SRC1 2010-2011.', 'films', '<video controls src="video/video_cm_andrew_jinn.mp4">video_cm_andrew_jinn.mp4</video>', 'video_cm_andrew_jinn_img.png', '', 'Antérieure à 2014', '', ''),
(174, 'BLOODY REPORT', 'Court-métrage réalisé par un groupe de la promotion SRC 2011-2012 dans le cadre du projet des 24h de l''audiovisuel (groupe "Elbi Prod''").', 'films', '<video controls src="video/video_cm_bloody_report.mp4">video_cm_bloody_report.mp4</video>', 'video_cm_bloody_report_img.png', '', 'Antérieure à 2014', '', ''),
(175, 'LA FAIM', 'Court-métrage réalisé par un groupe de la promotion SRC 2011-2012 dans le cadre du projet des 24h de l''audiovisuel (groupe "Omogen").', 'films', '<video controls src="video/video_cm_la_faim.mp4">video_cm_la_faim.mp4</video>', 'video_cm_la_faim_img.png', '', 'Antérieure à 2014', '', ''),
(176, '24H - MAKING OF', 'Réalisé par un groupe de production des 24h de l''audiovisuel de l''année 2011-2012, il va vous faire part de la bonne ambiance qui a régné durant cet évènement.', 'films', '<video controls src="video/video_making_of_24h.mp4">video_making_of_24h.mp4</video>', 'video_making_of_24h_img.png', '', 'Antérieure à 2014', '', ''),
(177, 'RED BOW TIES', 'Trailer du groupe de projet tuteuré Red Bow Ties de la promotion SRC2 2011-2012.', 'films', '<video controls src="video/redbowties.mp4">redbowties.mp4</video>', 'video_teaser_rbt_img.png', '', 'Antérieure à 2014', '', ''),
(178, 'PUB SRCARD', 'Publicité réalisée par des étudiants SRC de la promotion 2009-2011.', 'films', '<video controls src="video/video_pub_srcard.mp4">video_pub_srcard.mp4</video>', 'video_pub_srcard_img.png', '', 'Antérieure à 2014', '', ''),
(179, 'WEBTV MASTER''U', 'WebTV réalisée par des étudiants du groupe de projet tuteuré Master''U 2010-2011.', 'films', '<video controls src="video/video_webtv_masteru.mp4">video_webtv_masteru.mp4</video>', 'video_webtv_masteru_img.png', '', 'Antérieure à 2014', '', ''),
(180, 'L''IUT EN 3D', 'Présentation 3D de l''IUT d''ELbeuf réalisée par le groupe de projet tuteuré Département SRC 2010-2011.', '3D', '<video controls src="video/video_iut_3d.mp4">video_iut_3d.mp4</video>', 'video_iut_3d_img.png', '', 'Antérieure à 2014', '', ''),
(181, 'CLIP MUSICAL', 'Clip réalisé par un étudiant SRC2 de la promotion 2011-2012 grâce à un logiciel de montage/trucage vidéo et d''animation flash.', 'animations', '<video controls src="video/FHautekiet.mp4">FHautekiet.mp4</video>', 'video_clip_musical_fh_img.png', '', 'Antérieure à 2014', '', ''),
(182, 'CLIP VIDEO FLASH', 'Clip réalisé par un étudiant SRC2 de la promotion 2011-2012 grâce à un logiciel d''animation flash.', 'animations', '<video controls src="video/video_clip_flash_qv.mp4">video_clip_flash_qv.mp4</video>', 'video_clip_flash_qv_img.png', '', 'Antérieure à 2014', '', ''),
(183, 'CLIP MUSICAL FLASH', 'Clip musical réalisé par une étudiante SRC2 de la promotion 2011-2012 grâce à un logiciel d''animation flash.', 'animations', '<video controls src="video_clip_music_cv.mp4">video_clip_music_cv.mp4</video>', 'video_clip_music_cv_img.png', '', 'Antérieure à 2014', '', ''),
(184, 'AFFICHES', 'Réalisations d''étudiants composées pour diverses demandes infographique.', 'affiches', '', '6.jpg', 'affiche grph (1).jpg, affiche grph (2).jpg, affiche grph (3).jpg, affiche grph (4).jpg, affiche grph (6).jpg, affiche grph (7).jpg, affiche grph (8).jpg', 'Antérieure à 2014', '', ''),
(185, 'TYPOGRAPHIE', 'Les étudiants ayant réalisés ces travaux ont eu pour consigne la mise en scène graphique d''un fragment de texte (jeux typo-graphiques et design d''édition).', 'affiches', '', 'tjtfjtjftdrjh (5).jpg', 'tjtfjtjftdrjh (1).jpg, tjtfjtjftdrjh (2).jpg, tjtfjtjftdrjh (3).jpg, tjtfjtjftdrjh (4).jpg, tjtfjtjftdrjh (6).jpg, tjtfjtjftdrjh (7).jpg, tjtfjtjftdrjh (8).jpg, tjtfjtjftdrjh (9).jpg', 'Antérieure à 2014', '', ''),
(186, 'ART 2 RUE', 'Elaboration d''une identité institutionnelle complète (design de communication, d''espace et d''objet) associée à l''univers urbain et sousterrain.', 'affiches', '', 'r-tu-tr-t (2).jpg', 'r-tu-tr-t (1).jpg, r-tu-tr-t (3).jpg, r-tu-tr-t (4).jpg, r-tu-tr-t (5).jpg, r-tu-tr-t (6).jpg, r-tu-tr-t (7).jpg', 'Antérieure à 2014', '', ''),
(187, 'ARTS', 'Elaboration d''une identité institutionnelle complète (design de communication, d''espace et d''objet) associée à une architecture moderne.', 'identité visuelle', '', 'akd (19).jpg', 'akd (18).jpg, akd (20).jpg, akd (21).jpg, akd (17).jpg, akd (16).jpg, akd (15).jpg, akd (14).jpg, akd (13).jpg, akd (12).jpg, akd (11).jpg, akd (10).jpg, akd (9).jpg, akd (8).jpg, akd (7).jpg, akd (6).jpg, akd (5).jpg, akd (4).jpg, akd (3).jpg, akd (2).jpg, akd (1).jpg', 'Antérieure à 2014', '', ''),
(188, 'ART STATION', 'Elaboration d''une identité institutionnelle complète (design de communication, d''espace et d''objet) associée à une station de métro.', 'identité visuelle', '', 'tfhtj (6).jpg', 'tfhtj (1).jpg, tfhtj (2).jpg, tfhtj (3).jpg, tfhtj (4).jpg, tfhtj (5).jpg, tfhtj (7).jpg, tfhtj (8).jpg', 'Antérieure à 2014', '', ''),
(189, 'LE CARGO', 'Elaboration d''une identité institutionnelle complète (design de communication, d''espace et d''objet) associée à une épave d''un cargo échoué.', 'identité visuelle', '', 'erftjjyft (1).jpg', 'erftjjyft (2).jpg, erftjjyft (3).jpg, erftjjyft (4).jpg', 'Antérieure à 2014', '', ''),
(190, 'GUGGENHEIM', 'Elaboration d''une identité institutionnelle complète (design de communication, d''espace et d''objet) associée à une architecture internationale.', 'identité visuelle', '', 'esesefsesffesj (1).jpg', 'esesefsesffesj (2).jpg, esesefsesffesj (3).jpg, esesefsesffesj (4).jpg, esesefsesffesj (5).jpg', 'Antérieure à 2014', '', ''),
(191, 'SUB', 'Elaboration d''une identité institutionnelle complète (design de communication, d''espace et d''objet) associée à une architecture ressemblant à un réacteur nucléaire.', 'identité visuelle', '', 'hrththhhh (1).jpg', 'hrththhhh (2).jpg, hrththhhh (3).jpg, hrththhhh (4).jpg, hrththhhh (5).jpg, hrththhhh (6).jpg, hrththhhh (7).jpg, hrththhhh (8).jpg, hrththhhh (9).jpg', 'Antérieure à 2014', '', ''),
(192, 'SEMIOLOGIE TABASCO', 'Réalisation de compositions infographiques à partir d''un concept publicitaire choisi par chaque étudiant.', 'affiches', '', 'drtfjxfjtcjtf (8).jpg', 'drtfjxfjtcjtf (9).jpg, drtfjxfjtcjtf (7).jpg', 'Antérieure à 2014', '', ''),
(193, 'autre', '', 'affiches', '', 'gdrhdrhd (4).jpg', 'gdrhdrhd (1).jpg', 'Antérieure à 2014', '', ''),
(195, 'SEMIOLOGIE BOISSONS', 'Réalisation de compositions infographiques à partir d''un concept publicitaire choisi par chaque étudiant.', 'affiches', '', 'drtfjxfjtcjtf (1).jpg', 'drtfjxfjtcjtf (2).jpg, drtfjxfjtcjtf (3).jpg', 'Antérieure à 2014', '', ''),
(196, 'SEMIOLOGIE FABER-CASTELL', 'Réalisation de compositions infographiques à partir d''un concept publicitaire choisi par chaque étudiant.', 'affiches', '', 'drtfjxfjtcjtf (5).jpg', 'drtfjxfjtcjtf (4).jpg, drtfjxfjtcjtf (6).jpg', 'Antérieure à 2014', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `mmi_type`
--

CREATE TABLE IF NOT EXISTS `mmi_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) NOT NULL,
  `domaine` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Contenu de la table `mmi_type`
--

INSERT INTO `mmi_type` (`id`, `type`, `domaine`) VALUES
(2, 'sites', 'programmation'),
(4, 'applications', 'programmation'),
(5, 'jeux', 'programmation'),
(8, 'logos', 'infographie'),
(10, 'affiches', 'infographie'),
(11, '3D', 'infographie'),
(13, 'photos', 'audiovisuel'),
(14, 'films', 'audiovisuel'),
(15, 'animations', 'audiovisuel'),
(24, 'maquette', 'infographie'),
(27, 'carte de visite', 'infographie'),
(28, 'identité visuelle', 'infographie');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
