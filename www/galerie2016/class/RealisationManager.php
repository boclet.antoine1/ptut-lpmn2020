<?php
class RealisationManager{

  private $db;

  public function __construct($db)
  {
    $this->setDb($db);
  }

  public function setDb(PDO $db)
  {
    $this->db = $db;
  }

  public function add(Realisation $realisation)
  {
    $nb = $this->db->prepare('INSERT INTO mmi_realisation
      SET id = :id,
      realisation = :realisation,
      description = :description,
      lien = :lien,
      auteur = :auteur,
      type = :type,
      code = :code,
      promotion = :promotion,
      img_principale = :img_principale,
      img_secondaire = :img_secondaire
   ');

    $nb->bindValue(':id', $realisation->id(), PDO::PARAM_INT);
    $nb->bindValue(':realisation', $realisation->realisation(), PDO::PARAM_INT);
    $nb->bindValue(':description', $realisation->description(), PDO::PARAM_INT);
    $nb->bindValue(':lien', $realisation->lien(), PDO::PARAM_INT);
    $nb->bindValue(':auteur', $realisation->auteur(), PDO::PARAM_INT);
    $nb->bindValue(':type', $realisation->type(), PDO::PARAM_INT);
    $nb->bindValue(':code', $realisation->code(), PDO::PARAM_INT);
    $nb->bindValue(':promotion', $realisation->promotion(), PDO::PARAM_INT);
    $nb->bindValue(':img_principale', $realisation->img_principale(), PDO::PARAM_INT);
    $nb->bindValue(':img_secondaire', $realisation->img_secondaire(), PDO::PARAM_INT);

    $nb->execute();
    return $nb;
  }

  public function update(Realisation $realisation)
  {
    $nb = $this->db->prepare('UPDATE mmi_realisation
      SET id = :id,
      realisation = :realisation,
      description = :description,
      lien = :lien,
      auteur = :auteur,
      type = :type,
      code = :code,
      promotion = :promotion,
      img_principale = :img_principale,
      img_secondaire = :img_secondaire
      WHERE id = :id
    ');

    $nb->bindValue(':id', $realisation->id());
    $nb->bindValue(':realisation', $realisation->realisation(), PDO::PARAM_INT);
    $nb->bindValue(':description', $realisation->description(), PDO::PARAM_INT);
    $nb->bindValue(':type', $realisation->type(), PDO::PARAM_INT);
    $nb->bindValue(':code', $realisation->code(), PDO::PARAM_INT);
    $nb->bindValue(':img_principale', $realisation->img_principale(), PDO::PARAM_INT);
    $nb->bindValue(':img_principale', $realisation->img_principale(), PDO::PARAM_INT);
    $nb->bindValue(':img_secondaire', $realisation->img_secondaire(), PDO::PARAM_INT);
    $nb->bindValue(':promotion', $realisation->promotion(), PDO::PARAM_INT);
    $nb->bindValue(':auteur', $realisation->auteur(), PDO::PARAM_INT);
    $nb->bindValue(':lien', $realisation->lien(), PDO::PARAM_INT);

    $nb->execute();
    return $nb;
  }

  public function delete(Realisation $realisation)
  {
    $this->db->exec('DELETE FROM mmi_realisation WHERE id = '.$realisation->id());
  }

  public function get($id)
  {
    $id = (int) $id;

    $req = $this->db->query('SELECT * FROM mmi_realisation WHERE id = '.$id);
    $donnees = $req->fetch(PDO::FETCH_ASSOC);

    return new Realisation($donnees);
  }

  public function getList()
  {
    $realisations = [];

    $req = $this->db->query('SELECT * FROM mmi_realisation ORDER BY realisation');

    while ($donnees = $req->fetch(PDO::FETCH_ASSOC))
    {
      $realisations[] = new Realisation($donnees);
    }

    return $realisations;
  }
}
