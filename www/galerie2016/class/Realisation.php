<?php
class Realisation{
	private $id;

	private $realisation;
	private $description;
	private $type;
	private $code;

	private $promotion;
	private $auteur;
	private $lien;

	private $img_principale;
	private $img_secondaire;

	public function __construct(array $tab)
	{
		$this->hydrate($tab);
	}

	public function hydrate(array $donnees)
	{
	  foreach ($donnees as $key => $value)
	  {
	    $method = 'set'.ucfirst($key);
	    if (method_exists($this, $method))
	    {
      		$this->$method($value);
	    }
	  }
	}

	// Getters
	public function id(){return $this->id;}
	public function realisation(){return $this->realisation;}
	public function description(){return $this->description;}
	public function lien(){return $this->lien;}
	public function auteur(){return $this->auteur;}
	public function type(){return $this->type;}
	public function code(){return $this->code;}
	public function promotion(){return $this->promotion;}
	public function img_principale(){return $this->img_principale;}
	public function img_secondaire(){return $this->img_secondaire;}

	// Setters
	public function setId($val){$this->id=$val;}
	public function setRealisation($val){$this->realisation=$val;}
	public function setDescription($val){$this->description=$val;}
	public function setLien($val){$this->lien=$val;}
	public function setAuteur($val){$this->auteur=$val;}
	public function setType($val){$this->type=$val;}
	public function setCode($val){$this->code=$val;}
	public function setPromotion($val){$this->promotion=$val;}
	public function setImg_principale($val){$this->img_principale=$val;}
	public function setImg_secondaire($val){$this->img_secondaire=$val;}

	// Méthode
	public function affiche(){
		echo
			"id: ".$this->id."<br/>".
			"realisation: ".$this->realisation."<br/>".
			"description: ".$this->description."<br/>".
			"lien: ".$this->lien."<br/>".
			"auteur: ".$this->auteur."<br/>".
			"type: ".$this->type."<br/>".
			"code: ".$this->code."<br/>".
			"promotion: ".$this->promotion."<br/>".
			"img_principale: ".$this->img_principale."<br/>".
			"img_secondaire: ".$this->img_secondaire."<br/><br/>"
		;
	}

	public function ajouter(){

	}

	// Destructeur
	public function __destruct(){

	}
}
