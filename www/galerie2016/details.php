<?php
	require('admin/connect.php');
	// CLASSES ET GESTIONNAIRE
	function chargerClasse($classe){
		require 'class/'.$classe.'.php';
	}
	spl_autoload_register('chargerClasse');

	$id = $_POST['id'];

	$RealisationManager = new RealisationManager($db);
	$Realisation = $RealisationManager->get($id);
	// $Realisation = new Realisation($_POST);

	$realisation = $Realisation->realisation();
	$description = $Realisation->description();
	$type = $Realisation->type();
	switch ($type) {
		case 'animations':
		case 'films':
		case 'photos':
			$domaine = 'audiovisuel';
			break;
		case '3D':
		case 'affiches':
		case 'carte de visite':
		case 'identité visuelle':
		case 'logos':
		case 'maquette':
			$domaine = 'infographie';
			break;
		case 'applications':
		case 'jeux':
		case 'sites':
			$domaine = 'programmation';
			break;
	}
	$code = $Realisation->code();
	$promotion = $Realisation->promotion();
	$auteur = $Realisation->auteur();
	$lien = $Realisation->lien();
	$img_principale = $Realisation->img_principale();
	$img_secondaire = explode(", ", $Realisation->img_secondaire());
	$nbImg = 0;
	$site = "";
?>
<?php include 'head.php'; ?>
	<link rel="stylesheet" type="text/css" href="css/details.css">
	<script type="text/javascript" src="js/details.js"></script>
</head>

<section class="details">
	<?php
	if($code != ""){
		// if ($type == "sites") {
			// $site = 1;
		// }
		// else {
			echo $code;
		// }
	}
	else { ?>
		<div id="slider">
			<ul>
				<?php if ($img_principale != 'defaut.jpg'){ ?>
					<li class="slide"><a target="_blank" href="img/realisation/<?= $img_principale ?>"><img src="img/realisation/<?= $img_principale ?>" alt="<?= $realisation ?>" /></a></li>
				<?php }
				foreach($img_secondaire as $value){ ?>
					<?php if (is_file('img/realisation/'.$value)){
						$nbImg++; ?>
						<li class="slide"><a target="_blank" href="img/realisation/<?= $value ?>"><img src="img/realisation/<?= $value ?>" alt="<?= $realisation ?>" /></a></li>
					<?php } ?>
				<?php } ?>
			</ul>
			<div id="navigation">
				<?php if ($nbImg != 0){
						if ($img_principale != 'defaut.jpg'){ ?>
						<img class="illustration" src="img/realisation/miniature/<?= $img_principale ?>" alt="<?= $realisation ?>" />
					<?php }
					foreach($img_secondaire as $value){ ?>
						<?php if (is_file('img/realisation/miniature/'.$value)){ ?>
							<img class="illustration" src="<?= 'img/realisation/miniature/'.$value ?>" alt="<?= $realisation ?>" />
						<?php }
					}
				} ?>
			</div>
		</div>
	<?php } ?>

	<div id="texte">
		<h1><?= $realisation ?></h1>
		<?php if ($domaine != ""): ?>
			<p><span class="bold">Domaine:</span> <?= $domaine ?></p>
		<?php endif; ?>
		<p><span class="bold">Type:</span> <?= $type ?></p>
		<br/>
		<p> <?= $description ?> </p>
		<br/>
		<?php if ($site == 1): ?>
			<a href="<?= $code ?>">Lien vers le site</a>
		<?php endif; ?>
		<p><span class="bold">Promotion:</span> <?= $promotion ?></p>
		<?php if ($auteur){ ?>
			<p><span class="bold">Auteur(s):</span>
				<?php if ($lien){ ?>
					<a href="<?= $lien ?>" target="_blank"><?= $auteur ?></a>
				<?php }
				else {
					echo $auteur;
				} ?>
			</p>
		<?php } ?>
		<br/><br/>
		<a class="retour" href="index.php">Retour au à la galerie</a>
	</div>
</section>

</body>
</html>
