<?php
require('miniature.php');

// FORMULAIRE
// ADD
if (isset($_POST['addRealisation'])	&& isset($_POST['realisation']))
{
	if(!strstr( $_POST['description'], "<br />" )) {
		$_POST['description'] = nl2br($_POST['description']);
	}

	$realisation = new Realisation($_POST);

	$nb = $RealisationManager->add($realisation);

	if ($_FILES['file_principale']['error'] != UPLOAD_ERR_NO_FILE) // IMAGE PRINC
	{
		$FILES = $_FILES;
		$fichier_nom = 'file_principale';

		$dossier_fichier_chemin = "../img/realisation/";
		$nom = $_FILES['file_principale']['name'];
		$image_chemin = $dossier_fichier_chemin.$nom;
		move_uploaded_file($_FILES['file_principale']['tmp_name'], $image_chemin);

		$dossier_image_chemin = "miniature/";
		$miniature_dimension_max = 200;
		$mode = "full";
		miniature($FILES, $fichier_nom, $dossier_fichier_chemin, $dossier_image_chemin, $miniature_dimension_max, $mode);
	}

	if ($_FILES['file_secondaire']['error'] != UPLOAD_ERR_NO_FILE) // IMAGE SEC
	{
		$FILES = $_FILES;
		$fichier_nom = 'file_secondaire';

		$dossier_fichier_chemin = "../img/realisation/";
		$nom = $_FILES['file_secondaire']['name'];
		$image_chemin = $dossier_fichier_chemin.$nom;
		move_uploaded_file($_FILES['file_secondaire']['tmp_name'], $image_chemin);

		$dossier_image_chemin = "miniature/";
		$miniature_dimension_max = 200;
		$mode = "full";
		miniature($FILES, $fichier_nom, $dossier_fichier_chemin, $dossier_image_chemin, $miniature_dimension_max, $mode);
	}

}
elseif(isset($_POST['addType']) && isset($_POST['type']))
{
	$type = $_POST['type'];
	$domaine = $_POST['domaine'];
	$promotionAdd = $db->exec("INSERT INTO mmi_type(type, domaine) VALUES ('$type', '$domaine');");
	// if($promotionAdd){
	// 	echo "Ajout du type réussit";
	// }
	// else{
	// 	echo "Échec de l'ajout du type";
	// }
}
elseif(isset($_POST['addPromotion']) && isset($_POST['promotion']))
{
	$promotion = $_POST['promotion'];
	$promotionAdd = $db->exec("INSERT INTO mmi_promotion(promotion) VALUES ('$promotion');");
	// if($promotionAdd){
	// 	echo "Ajout du promotion réussit";
	// }
	// else{
	// 	echo "Échec de l'ajout du promotion";
	// }
}

// UP
elseif (isset($_POST['upRealisation']))
{

	if ($_FILES['file_principale']['error'] != UPLOAD_ERR_NO_FILE) // IMAGE PRINC
	{
		$FILES = $_FILES;
		$fichier_nom = 'file_principale';

		$dossier_fichier_chemin = "../img/realisation/";
		$nom = $_FILES['file_principale']['name'];
		$image_chemin = $dossier_fichier_chemin.$nom;
		move_uploaded_file($_FILES['file_principale']['tmp_name'], $image_chemin);

		$dossier_image_chemin = "miniature/";
		$miniature_dimension_max = 200;
		$mode = "full";
		miniature($FILES, $fichier_nom, $dossier_fichier_chemin, $dossier_image_chemin, $miniature_dimension_max, $mode);
		// miniature(le ficher, sa taille finale, sa destination)
	}

	if ($_FILES['file_secondaire']['error'] != UPLOAD_ERR_NO_FILE) // IMAGE SEC
	{
		$FILES = $_FILES;
		$fichier_nom = 'file_secondaire';

		$dossier_fichier_chemin = "../img/realisation/";
		$nom = $_FILES['file_secondaire']['name'];
		$image_chemin = $dossier_fichier_chemin.$nom;
		move_uploaded_file($_FILES['file_secondaire']['tmp_name'], $image_chemin);

		$dossier_image_chemin = "miniature/";
		$miniature_dimension_max = 200;
		$mode = "full";
		miniature($FILES, $fichier_nom, $dossier_fichier_chemin, $dossier_image_chemin, $miniature_dimension_max, $mode);
	}

	if(!strstr( $_POST['description'], "<br />" )) {
		$_POST['description'] = nl2br($_POST['description']);
	}
	$realisation_actuel = $RealisationManager->get($_POST['id']); // récupération du realisation actuel dans la bdd à partir de l'id passé
	$realisation_nouveau = new Realisation($_POST); // création du nouveau realisation à partir de POST

	$realisation_actuel_img_principale = $realisation_actuel->img_principale();
	$realisation_nouveau_img_principale = $realisation_nouveau->img_principale();
	$realisation_actuel_img_secondaire = $realisation_actuel->img_secondaire();
	$realisation_nouveau_img_secondaire = $realisation_nouveau->img_secondaire();

	$dossier_chemin = "../img/realisation/";

	if ($realisation_actuel_img_principale) {
		if (!$realisation_nouveau_img_principale) {
			$img_principale_chemin = $dossier_chemin.$realisation_actuel_img_principale;
			unlink($img_principale_chemin);
			$img_principale_miniature_chemin = $dossier_chemin."miniature/".$realisation_actuel_img_principale;
			unlink($img_principale_miniature_chemin);
		}
	}

	if ($realisation_actuel_img_secondaire) {
		$realisation_actuel_img_secondaire = explode (", ", $realisation_actuel_img_secondaire);

		foreach ($realisation_actuel_img_secondaire as $key => $value){
			if (!strstr ($realisation_nouveau_img_secondaire , $realisation_actuel_img_secondaire[$key])) {
				$img_secondaire_chemin = $dossier_chemin.$realisation_actuel_img_secondaire[$key];
				unlink($img_secondaire_chemin);
				$img_secondaire_miniature_chemin = $dossier_chemin."miniature/".$realisation_actuel_img_secondaire[$key];
				unlink($img_secondaire_miniature_chemin);
			}
		}
	}

	$nb = $RealisationManager->update($realisation_nouveau);

	// if($nb){
	// 	echo "modification du realisation réussit";
	// }
	// else{
	// 	echo "Échec de la modification du realisation";
	// }
}

// DEL
elseif (isset($_POST['delRealisation'])	&& isset($_POST['id']))
{
	$realisation = new Realisation($_POST);
	$nb = $RealisationManager->delete($realisation);

	// if($nb){
	// 	echo "suppression du realisation réussit";
	// }
	// else{
	// 	echo "Échec de la suppression du realisation";
	// }

	$dossier_chemin = "../img/realisation/";

	if (!empty($_POST['img_principale'])){
		$img_principale_chemin = $dossier_chemin.$_POST['img_principale'];
		unlink($img_principale_chemin);

		$img_principale_miniature_chemin = $dossier_chemin."miniature/".$_POST['img_principale'];
		unlink($img_principale_miniature_chemin);
	}

	if (!empty($_POST['img_secondaire'])){
		$img_secondaire_list = explode(', ', $_POST['img_secondaire']);
		foreach ($img_secondaire_list as $key => $value) {
			$img_secondaire_chemin = $dossier_chemin.$img_secondaire_list[$key];
			unlink($img_secondaire_chemin);

			$img_secondaire_miniature_chemin = $dossier_chemin."miniature/".$img_secondaire_list[$key];
			unlink($img_secondaire_miniature_chemin);
		}
	}
}
elseif(isset($_POST['delType']) && isset($_POST['type']))
{
	$type = $_POST['type'];
	$nb = $db->exec("DELETE FROM mmi_type WHERE type = '$type'");
	// if($nb){
	// 	echo "Suppression du type réussit";
	// }
	// else{
	// 	echo "Échec de la suppression du type";
	// }
}
elseif(isset($_POST['delPromotion']) && isset($_POST['promotion']))
{
	$promotion = $_POST['promotion'];
	$nb = $db->exec("DELETE FROM mmi_promotion WHERE promotion = '$promotion'");
	// if($nb){
	// 	echo "Suppression de la promotion réussit";
	// }
	// else{
	// 	echo "Échec de la suppression de la promotion";
	// }
}

?>
