<?php
	// include_once("../alyticstracking.php").

	// CONNEXION À LA BASE DE DONNÉE
	require('connect.php');

	// CLASSES ET GESTIONNAIRE
	function chargerClasse($classe){
		require '../class/'.$classe.'.php';
	}
	spl_autoload_register('chargerClasse');

	$RealisationManager = new RealisationManager($db);

	require('formulaires.php');
	require('affichages.php');
?>

<!DOCTYPE>
<html>
<head>
	<!-- INFOS -->
	<title>Page d'administration</title>
	<meta name="author" content="Valink">

	<!-- ENCODAGE -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<!-- LIBRAIRIES -->
	<link rel="stylesheet" type="text/css" href="../font/font-awesome-4.4.0/css/font-awesome.min.css">
	<script src="../js/jquery.js"></script>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../css/admin.css">

	<!-- js -->
	<script src="../js/admin.js"></script>
</head>
<body>

<h1><a href="index.php">Page d'administration</a></h1>

<h2>Caractéristiques des realisations</h2>
<section id="caracteristiques">
	<fieldset id="type_fieldset">
		<form method="POST" action="?addType">
			<input type="submit" name="addType" value="Ajouter type" />
			<input type="text" name="type" placeholder="Type" />

			<select name="domaine">
				<?php
					$get_domaine = get_domaine($db);
					while ($donnees_domaine = $get_domaine->fetch(PDO::FETCH_ASSOC)){
				?>

				<option><?= $donnees_domaine['domaine']; ?></option>

				<?php
				  }
					$get_domaine->closeCursor();
				?>
			</select>

		</form>

		<form method="POST" action="?delType">
			<input type="submit" name="delType" value="Supprimer type" />
			<select name="type">
				<option></option>
				<?php $get_type = get_type($db);
				while ($donnees = $get_type->fetch(PDO::FETCH_ASSOC)){?>
					<option><?= $donnees['type'] ?></option>
				<?php } $get_type->closeCursor(); ?>
			</select>
		</form>
	</fieldset>

	<fieldset id="promotion_fieldset">
		<form method="POST" action="?addPromotion">
			<input type="submit" name="addPromotion" value="Ajouter période" />
			<input type="text" name="promotion" placeholder="Période" />
		</form>

		<form method="POST" action="?delPromotion">
			<input type="submit" name="delPromotion" value="Supprimer période" />
			<select name="promotion">
				<option></option>
				<?php $get_promotion = get_promotion($db);
				while ($donnees = $get_promotion->fetch(PDO::FETCH_ASSOC)){?>
					<option><?= $donnees['promotion'] ?></option>
				<?php } $get_promotion->closeCursor(); ?>
			</select>
		</form>
	</fieldset>
</section>

<h2>Realisations</h2>
<section id="realisations">
	<table>
		<thead>
			<tr>
				<th>Id</th>
				<th>Realisation</th>
				<th>Description</th>
				<th>Type</th>
				<th>Code</th>
				<th>Couverture</th>
				<th></th>
				<th>Illustration(s)</th>
				<th></th>
				<th>Promotion</th>
				<th>Auteur</th>
				<th>Lien</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$realisation_list = $RealisationManager->getList();
				foreach ($realisation_list as $realisation_key => $value) {
			?>
			<tr>
				<form method="POST" action="?up||delRealisation" enctype="multipart/form-data">
					<td><input name="id" value="<?= $realisation_list[$realisation_key]->id() ?>" class="nombre" /></td>
					<td><input name="realisation" value="<?= $realisation_list[$realisation_key]->realisation() ?>" /></td>
					<td><textarea name="description" ><?= $realisation_list[$realisation_key]->description() ?></textarea></td>
					<td class="type_cell">
						<?php
							$type_tab = explode(", ", $realisation_list[$realisation_key]->type());
							foreach ($type_tab as $type_key => $value){ ?>
								<select class="type_select">
									<option><?= $type_tab[$type_key] ?></option>
									<?php
										$get_type = get_type($db);
										while ($donnees = $get_type->fetch(PDO::FETCH_ASSOC)){
									?>
										<option><?= $donnees['type'] ?></option>
									<?php
										}
										$get_type->closeCursor();
									?>
								</select>
							<?php } ?>
						<i class="type_add fa fa-plus"></i>
						<i class="type_minus fa fa-minus"></i>
						<input class="type_value" name="type" value="<?= $realisation_list[$realisation_key]->type() ?>">
					</td>
					<td><textarea name="code" ><?= $realisation_list[$realisation_key]->code() ?></textarea></td>
					<td>
						<input name="file_principale" class="dropper" type="file" accept="image/*" />
						<input class="chaine" name="img_principale" value="<?= $realisation_list[$realisation_key]->img_principale() ?>" />
					</td>
					<td class="img_principale">
						<?php if($realisation_list[$realisation_key]->img_principale() != NULL){ ?>
							<!-- <a class="draggable" target="_blank" href="<?= '../img/realisation/'.$realisation_list[$realisation_key]->img_principale() ?>"> -->
								<img src="<?= '../img/realisation/miniature/'.$realisation_list[$realisation_key]->img_principale() ?>" />
							<!-- </a> -->
						<?php } ?>
					</td>
					<td>
						<input name="file_secondaire" class="dropper" type="file" accept="image/*" />
						<textarea class="chaine" name="img_secondaire" /><?= $realisation_list[$realisation_key]->img_secondaire() ?></textarea>
					</td>
					<td class="img_secondaire">
						<?php $img_secondaire = explode(", ", $realisation_list[$realisation_key]->img_secondaire());
						foreach($img_secondaire as $value){ ?>
							<?php if($value != NULL){ ?>
								<!-- <a class="draggable" target="_blank" href="<?= '../img/realisation/'.$value ?>"> -->
									<img src="<?= '../img/realisation/miniature/'.$value ?>" />
								<!-- </a> -->
							<?php }
						} ?>
					</td>
					<td class="promotion_cell">
						<select name="promotion" class="promotion_select">
							<option><?= $realisation_list[$realisation_key]->promotion() ?></option>
							<?php
								$get_promotion = get_promotion($db);
								while ($donnees = $get_promotion->fetch(PDO::FETCH_ASSOC)){
							?>
								<option><?= $donnees['promotion'] ?></option>
							<?php
								}
								$get_promotion->closeCursor();
							?>
						</select>
					</td>
					<td class="auteur_cell"><input class="auteur_value" name="auteur" value="<?= $realisation_list[$realisation_key]->auteur() ?>"></td>
					<td><input name="lien" value="<?= $realisation_list[$realisation_key]->lien() ?>" class="nombre" /></td>
					<td class="actionCell">
						<button id="trash" type="submit" name="upRealisation"><i class="fa fa-pencil"></i></button>
						<button id="pen" type="submit" name="delRealisation" onclick="if(window.confirm('Voulez-vous vraiment supprimer la realisation <?= $realisation_list[$realisation_key]->realisation() ?> ?')){return true;}else{return false;}"><i class="fa fa-trash-o"></i></button>
					</td>
			</form>
			</tr>
			<?php
				}
			?>

		</tbody>

		<tfoot>
			<tr>
				<form method="POST" action="?add||resRealisation enctype" enctype="multipart/form-data" >
					<th><input type="text" name="id" placeholder="Id" class="nombre"/></th>
					<th><input type="text" name="realisation" placeholder="Realisation" /></th>
					<th><textarea type="text" name="description" placeholder="Description" ></textarea></th>
					<th class="type_cell">
						<select class="type_select">
							<option>Type</option>
							<?php
								$get_type = get_type($db);
								while ($donnees = $get_type->fetch(PDO::FETCH_ASSOC)){
							?>
								<option><?= $donnees['type'] ?></option>
							<?php
								}
								$get_type->closeCursor();
							?>
						</select>
						<i class="type_add fa fa-plus"></i>
						<i class="type_minus fa fa-minus"></i>
						<input class="type_value" name="type" value="">
					</th>
					<th><textarea type="text" name="code" placeholder="Code" ></textarea></th>
					<th>
						<input name="file_principale" class="dropper" type="file" accept="image/*" />
						<input type="text" name="img_principale" placeholder="Couverture" />
					</th>
					<th class="img_principale"> </th>
					<th>
						<input name="file_secondaire" class="dropper" type="file" accept="image/*" />
						<textarea  name="img_secondaire" placeholder="Autres images" /></textarea></th>
					</th>
					<th class="img_secondaire"> </th>
					<th class="promotion_cell">
						<select name="promotion" class="promotion_select">
							<option></option>
							<?php
								$get_promotion = get_promotion($db);
								while ($donnees = $get_promotion->fetch(PDO::FETCH_ASSOC)){
							?>
								<option><?= $donnees['promotion'] ?></option>
							<?php
								}
								$get_promotion->closeCursor();
							?>
						</select>
					</th>
					<th class="auteur_cell"><input class="auteur_value" name="auteur" value=""></th>
					<th><input type="text" name="lien" placeholder="Lien" class="nombre" /></th>
					<th class="actionCell">
						<button class="plus" type="submit" name="addRealisation"><i class="fa fa-plus"></i></button>
						<button type="submit" name="resRealisation"><i class="fa fa-refresh"></i></button>
						<!-- mettre type="reset" et un evenement js pour retirer la miniature -->
					</th>
				</form>
			</tr>
		</tfoot>
	</table>
</section>

</body>
</html>
