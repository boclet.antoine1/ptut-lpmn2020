<?php
// tri de la galerie

function get_realisation_domaine($db){
	$req = $db->query('SELECT DISTINCT domaine FROM mmi_realisation ORDER BY domaine');
	return $req;
}
function get_realisation_type($db){
	$req = $db->query('SELECT DISTINCT type FROM mmi_realisation ORDER BY type');
	return $req;
}
function get_realisation_promotion($db){
	$req = $db->query('SELECT DISTINCT promotion FROM mmi_realisation ORDER BY promotion');
	return $req;
}
function get_realisation_auteur($db){
	$req = $db->query('SELECT DISTINCT auteur FROM mmi_realisation ORDER BY auteur');
	return $req;
}

// formulaire de gestion des types, promotions, et auteur du panel admin
function get_domaine($db){
	$req = $db->query('SELECT DISTINCT domaine FROM mmi_type ORDER BY domaine');
	return $req;
}
function get_type($db){
	$req = $db->query('SELECT type FROM mmi_type ORDER BY type');
	return $req;
}
function get_type_domaine($db, $domaine){
	$req = $db->query("SELECT type FROM mmi_type WHERE domaine = '$domaine' ORDER BY type");
	return $req;
}
function get_promotion($db){
	$req = $db->query('SELECT promotion FROM mmi_promotion ORDER BY promotion');
	return $req;
}
function get_auteur($db){
	$req = $db->query('SELECT auteur FROM mmi_auteur ORDER BY auteur');
	return $req;
}
?>
