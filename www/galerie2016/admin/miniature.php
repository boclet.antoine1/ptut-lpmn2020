<?php

function miniature($FILES, $fichier_nom, $dossier_fichier_chemin, $dossier_image_chemin, $miniature_dimension_max, $mode){
	// echo($FILES ." ". $fichier_nom ." ". $dossier_fichier_chemin ." ". $miniature_dimension_max ." ". $mode. " aaa". $image_taille['mime'] ."&&&");

	if ($FILES[$fichier_nom]['error'] <= 0) // Si le ficiher ne comporte pas d'erreur
	{
		// if ($FILES[$fichier_nom]['size'] <= 8388608) // Si le fichier ne dépasse pas une certaine taille
		// {
			$image_nom_extension = $FILES[$fichier_nom]['name']; // on stock le nom du fichier
			$image_nom_extension_tab = explode('.', $image_nom_extension); // on sépare le nom et l'extension dans un tableau
			$image_nom = strtolower($image_nom_extension_tab[0]); // on stock l'extension
			$image_extension = strtolower($image_nom_extension_tab[1]); // on stock l'extension

			$extension_liste = array('jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
			$extension_liste_ie = array('jpg' => 'image/pjpeg', 'jpeg'=>'image/pjpeg');

			if ($image_extension == 'jpg' || $image_extension == 'jpeg' || $image_extension == 'png' || $image_extension == 'gif') // si l'extension est .jpg, .jpeg, .png ou .gif
			{
				$image_chemin = $dossier_fichier_chemin.$image_nom_extension;
				$image_taille = getimagesize($image_chemin); // on stock dans un tableau les caractéristiques de l'image à partir de son nom temporaire

				if($image_taille['mime'] == $extension_liste[$image_extension]  || $image_taille['mime'] == $extension_liste_ie[$image_extension]) // si le type MIME de l'image correspond
				{
					switch ($image_extension) {
						case 'jpg':
						case 'jpeg':
							$image_identifiant = imagecreatefromjpeg($image_chemin); // on creer une imager et on stock un identifiant d'image représentant une image obtenue à partir du nom temporaire
							break;
						case 'png':
							$image_identifiant = imagecreatefrompng($image_chemin); // on creer une imager et on stock un identifiant d'image représentant une image obtenue à partir du nom temporaire
							break;
						case 'gif':
							$image_identifiant = imagecreatefromgif($image_chemin); // on creer une imager et on stock un identifiant d'image représentant une image obtenue à partir du nom temporaire
							break;
					}

					$image_largeur = $image_taille[0];
					$image_hauteur = $image_taille[1];

					// PENSER A GERER LE CAS OU L'IMAGE SOURCE EST PLUS PETITE QUE LA MINIATURE
						// REDIMENSIONNEMENT POUR RENTRER DANS BLOC
						if ($mode == "ajust") {
							if ($image_largeur > $image_hauteur) { // Si le format de l'image est paysage
								$miniature_largeur = $miniature_dimension_max; // on stock la largeur choisie
								$miniature_hauteur = ( ($image_hauteur * (($miniature_largeur)/$image_largeur)) ); // on calcul la hauteur en multipliant la nouvelle hauteur par le ratio nouvelle largeur/Largeur de l'image
							}
							elseif ($image_hauteur > $image_largeur) { // Si le format de l'image est portrait
								$miniature_hauteur = $miniature_dimension_max; // on stock la hauteur choisie
								$miniature_largeur = ( ($image_largeur * (($miniature_hauteur)/$image_hauteur)) ); // on calcul la largeur en multipliant la nouvelle largeur par le ratio nouvelle Hauteur/Hauteur de l'image
							}
							else{  // Si le format de l'image est carré
								$miniature_largeur = $miniature_dimension_max;
								$miniature_hauteur = $miniature_dimension_max;
							}
						}

						// REDIMENSIONNEMENT POUR REMPLIR BLOC
						elseif ($mode == "full") {
							if ($image_largeur > $image_hauteur) { // Si le format de l'image est paysage
								$miniature_hauteur = $miniature_dimension_max; // on stock la hauteur choisie
								$miniature_largeur = ( ($image_largeur * (($miniature_hauteur)/$image_hauteur)) ); // on calcul la largeur en multipliant la nouvelle largeur par le ratio nouvelle Hauteur/Hauteur de l'image
							}
							elseif ($image_hauteur > $image_largeur) { // Si le format de l'image est portrait
								$miniature_largeur = $miniature_dimension_max; // on stock la largeur choisie
								$miniature_hauteur = ( ($image_hauteur * (($miniature_largeur)/$image_largeur)) ); // on calcul la hauteur en multipliant la nouvelle hauteur par le ratio nouvelle largeur/Largeur de l'image
							}
							else{  // Si le format de l'image est carré
								$miniature_largeur = $miniature_dimension_max;
								$miniature_hauteur = $miniature_dimension_max;
							}
						}

					$miniature = imagecreatetruecolor($miniature_largeur , $miniature_hauteur) or die ("Erreur"); // Crée une nouvelle image en couleurs vraies et stock une ressource représentant une image noire.
					imagecopyresampled($miniature , $image_identifiant  , 0,0, 0,0, $miniature_largeur, $miniature_hauteur, $image_largeur, $image_hauteur); // Copie, redimensionne, rééchantillonne une image

					$miniature_chemin = $dossier_fichier_chemin.$dossier_image_chemin.$image_nom_extension;

					switch ($image_extension) {
						case 'jpg':
						case 'jpeg':
							imagejpeg($miniature , $miniature_chemin, 100); // Enregistrement de la miniature
							break;
						case 'png':
							imagepng($miniature , $miniature_chemin, 9); // Enregistrement de la miniature
							break;
						case 'gif':
							imagegif($miniature , $miniature_chemin); // Enregistrement de la miniature
							break;
					}

					imagedestroy($miniature); // Détruit une image, libère toute la mémoire associée à l'image

				}
				else
				{
					echo 'Le type d\'image choisi n\'est pas pris en charge';
				}
			}
			else
			{
				echo 'L\'extension de l\'image est incorrecte';
			}
		// }
		// else
		// {
		// 	echo 'L\'image est trop lourde';
		// }
	}
	else
	{
		echo 'Erreur lors de l\'upload image';
	}
}
