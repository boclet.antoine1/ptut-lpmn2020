$(function(){
$(window).load(function(){

		$('#tri h2').click(function(){
			$('#tri .tri_liste').slideUp();
			$('#tri svg').css('transform', 'rotate(0)');

			if ($(this).next('ul').css('display') == "none") {
				$(this).children('svg').css('transform', 'rotate(90deg)');
				$(this).next('ul').slideDown();
			}
			else{
				$(this).children('svg').css('transform', 'rotate(0)');
				$(this).next('ul').slideUp();
			}
		});

		$('.tri_item').click(function(){
			var ul, li;
			if ($(this).text() == "TOUTES" || $(this).text() == "PROMOTIONS"){
				$('#catalogue_infos').fadeOut();

				if ($('#fil').html() != "&nbsp;"){
					$('#fil').fadeOut(function(){
						$(this).html("&nbsp;");
						$(this).toggle();
					});

					$('.realisation').fadeOut(function(){
						$('.realisation').fadeIn();
					});
				}
			}
			else{
				ul = $(this).parent().parent().attr("id");
				li = $(this).text();
				console.log(ul +" " + li);

				var fil, tri_titre_choisi, tri_liste_choisi;
				var realisation_nombre;
				tri_titre_choisi = $(this).parent().parent().prev('p').text();
				tri_liste_choisi = $(this).text();
				fil = tri_titre_choisi + " → " + tri_liste_choisi;

				if ($('#fil').text() != fil){
					$('#fil').fadeOut(function(){
						$(this).text(fil);
						$(this).fadeIn();

						realisation_nombre = $('.realisation input[name=\"'+ul+'\"][value*=\"'+li+'\"]').length;

						if (realisation_nombre != 0){
							$('.realisation').fadeOut(function(){
								$('#catalogue_infos').fadeOut();
								$('.realisation input[name=\"'+ul+'\"][value*=\"'+li+'\"]').parent().fadeIn();
							});
						}
						else{
							$('.realisation').fadeOut();
							$('#catalogue_infos').fadeIn();
						}

					});
				}
			}
		});

		$('.tri_domaine').click(function(){
			var ul, li;
			if ($(this).text() == "PROMOTIONS"){
				$('#catalogue_infos').fadeOut();

				if ($('#fil').html() != "&nbsp;"){
					$('#fil').fadeOut(function(){
						$(this).html("&nbsp;");
						$(this).toggle();
					});

					$('.realisation').fadeOut(function(){
						$('.realisation').fadeIn();
					});
				}
			}
			else{
				ul = "domaine";
				li = $(this).attr("id");
				console.log(li);

				var fil, tri_titre_choisi, tri_liste_choisi;
				var realisation_nombre;
				tri_liste_choisi = $(this).text();
				fil = " → " + tri_liste_choisi;

				if ($('#fil').text() != fil){
					$('#fil').fadeOut(function(){
						$(this).text(fil);
						$(this).fadeIn();

						realisation_nombre = $('.realisation input[name=\"'+ul+'\"][value*=\"'+li+'\"]').length;

						if (realisation_nombre != 0){
							$('.realisation').fadeOut(function(){
								$('#catalogue_infos').fadeOut();
								$('.realisation input[name=\"'+ul+'\"][value*=\"'+li+'\"]').parent().fadeIn();
							});
						}
						else{
							$('.realisation').fadeOut();
							$('#catalogue_infos').fadeIn();
						}

					});
				}
			}
		});

		$('footer').css('top', $(document).height());
		$('iframe').css('height', $(window).height());

});
});
