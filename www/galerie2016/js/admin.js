$(function(){

	/* ADMIN */
	$('#actualites').prev('h2').click(function(){
		$('#slider').toggle();
	});
	$('#realisations').prev('h2').click(function(){
		$('tbody').toggle();
	});
	$('#caracteristiques').prev('h2').click(function(){
		$('fieldset').toggle();
	});

	// SLIDER
	// ajout d'une image de fond
	$('#slider input[type="file"]').change(function(){
		$(this).next().val($(this).val());
	});


	// TABLE
		/* PLUSIEURS CARACTERISTIQUES */
		// PARFUM
		function change_auteur_value(){
			$('.auteur_select').change(function(){
				var auteur_value = "";
				$(this).nextAll('.auteur_value').val(auteur_value);
				$(this).parent('.auteur_cell').children('.auteur_select').each(function(){
					if ($(this).val() != "") {
						auteur_value += $(this).val() + ", ";
					}
				});
				auteur_value = auteur_value.substring(0, auteur_value.length-2);
				$(this).nextAll('.auteur_value').val(auteur_value);
			});
		}
		change_auteur_value();

		$('.auteur_add').click(function(){
			auteur_clone = $(this).prev('.auteur_select').clone(true);
			auteur_clone.children('option:first').text(' ');
			auteur_clone.insertBefore(this);
		});

		$('.auteur_minus').click(function(){
			if ($(this).parent('.auteur_cell').children('.auteur_select').length > 1) {
				$(this).parent('.auteur_cell').children('.auteur_select:last').remove();
				var auteur_value = "";
				$(this).next('.auteur_value').val(auteur_value);
				$(this).parent('.auteur_cell').children('.auteur_select').each(function(){
					if ($(this).val() != "") {
						auteur_value += $(this).val() + ", ";
					}
				});
				auteur_value = auteur_value.substring(0, auteur_value.length-2);
				$(this).next('.auteur_value').val(auteur_value);
			}
		});

		// TYPE
		function change_type_value(){
			$('.type_select').change(function(){
				var type_value = "";
				$(this).nextAll('.type_value').val(type_value);
				$(this).parent('.type_cell').children('.type_select').each(function(){
					if ($(this).val() != "") {
						type_value += $(this).val() + ", ";
					}
				});
				type_value = type_value.substring(0, type_value.length-2);
				$(this).nextAll('.type_value').val(type_value);
			});
		}
		change_type_value();

		$('.type_add').click(function(){
			type_clone = $(this).prev('.type_select').clone(true);
			type_clone.children('option:first').text(' ');
			type_clone.insertBefore(this);
		});

		$('.type_minus').click(function(){
			if ($(this).parent('.type_cell').children('.type_select').length > 1) {
				$(this).parent('.type_cell').children('.type_select:last').remove();
				var type_value = "";
				$(this).next('.type_value').val(type_value);
				$(this).parent('.type_cell').children('.type_select').each(function(){
					if ($(this).val() != "") {
						type_value += $(this).val() + ", ";
					}
				});
				type_value = type_value.substring(0, type_value.length-2);
				$(this).next('.type_value').val(type_value);
			}
		});

	// IMAGE
		// ajout d'une image
		$('table input[type="file"]').change(function(){
			// recupération de la liste des images secondaires
			var filenames = $(this).next().val();

			var virgule_suite = ', ';
			// si le champ est vide, pas besoin de mettre une virgule comme séparateur
			if(filenames == ''){
				virgule_suite = '';
			}

			// ajout du nom de l'image à la suite de la liste des images scondaires
			filenames += virgule_suite + $(this).val();
			$(this).next().val(filenames);

			// ajout de la miniature de la nouvelle image
			// $(this).parent().next().append('<a class="draggable" target="_blank" href=../img/realisation/' + $(this).val() + '><img src=../img/realisation/' + $(this).val() + ' /></a> ');
			$(this).parent().next().append('<img src=../img/realisation/' + $(this).val() + ' />');
		});

		// suppression de l'image cliquée
    $('img').click(function(){
    	// recupération du chemin/nom de l'image cliquée
        img_suppr = $(this).attr("src");
        // suppression du cheminde l'image
        img_suppr = img_suppr.replace("../img/realisation/miniature/", "");

        // récupération de la liste des images secondaires actuelles
        filenames = $(this).parent('td').prev('td').find('.chaine').val();

        // suppresion de l'image cliquée de la liste
        filenames = filenames.replace(img_suppr, "");

        // si l'image supprimée était en 1ère position de la liste, on supprime la virgule
        if(filenames.substring(0,2) == ", "){
            filenames = filenames.substring(2,filenames.length);
        }
        // si l'image supprimée était en milieu de liste, on remplace la double virgule par une simple
        filenames = filenames.replace(", , ", ", ");
        // si l'image supprimée était en dernière position de la liste, on supprime la virgule
        if(filenames.substring(filenames.length-2,filenames.length) == ", "){
            filenames = filenames.substring(0,filenames.length-2);
        }

        // remplacement de la liste des images secondaires actuelles par la nouvelle liste
        $(this).parent('td').prev('td').find('.chaine').val(filenames);

       	// suppression de la miniature de l'image cliquée
				$(this).remove();

  	});

		// $("button[name=addRealisation]").click(function(){
		$("button[name=addRealisation]").click(function(){
			date_ajout = new Date();
			date_ajout = date_ajout.getFullYear()+"-"+(date_ajout.getMonth()+1)+"-"+date_ajout.getDate() +" "+ date_ajout.getHours()+":"+date_ajout.getMinutes()+":"+date_ajout.getSeconds();
			$('#date_ajout').val(date_ajout);
		});


// ANCIENNE VERSION DND
// 	var dndHandler = {

// 	    draggable: null, // Propriété pointant vers l'élément en cours de déplacement

// 	    applyDragEvents: function(draggable) {

// 	        draggable.draggable = true;

// 	        var dndHandler = this; // Cette variable est nécessaire pour que l'événement « dragstart » ci-dessous accède facilement au namespace « dndHandler »

// 	        // draggable.addEventListener('dragstart', function(e) {
// 	        //     dndHandler.draggable = e.target; // On sauvegarde l'élément en cours de déplacement
// 	        //     e.dataTransfer.setData('text/plain', '');
// 	        // }, false);

// 			draggable.addEventListener('dragstart', function(e){
// 			    e.dataTransfer.setData('text/plain', $(this).children('img').attr('src'));
// 			}, false);
// 		},

// 		applyDropEvents: function(dropper) {

// 			var dndHandler = this; // Cette variable est nécessaire pour que l'événement « drop » ci-dessous accède facilement au namespace « dndHandler »

// 			dropper.addEventListener('dragover', function(e) {
// 			    e.preventDefault(); // Annule l'interdiction de drop
// 			    this.style.background = 'lightblue';
// 			}, false);
// 				// dropper.addEventListener('dragenter', function() {
// 				// }, false);
// 				dropper.addEventListener('dragleave', function() {
// 					this.style.background = 'none';
// 				}, false);

// 			// document.addEventListener('dragend', function() {
// 			// }, false);

// 			dropper.addEventListener('drop', function(e) {
// 			    e.preventDefault(); // Cette méthode est toujours nécessaire pour éviter une éventuelle redirection inattendue

// 				var filenames = $(this).val();

// 				var virgule_suite = ', ';
// 				if(filenames == ''){
// 					virgule_suite = '';
// 				}

// 			    if(e.dataTransfer.getData('text/plain')) {
// 				    filenames += virgule_suite + e.dataTransfer.getData('text/plain').replace('../img/realisation/', '') + ", ";
// 				    $(this).parent().next('td').append('<a class="draggable" target="_blank" href="' + e.dataTransfer.getData('text/plain') + '"><img src="' + e.dataTransfer.getData('text/plain') + '" /></a> ');
// 				}

//  				else{
// 					var files = e.dataTransfer.files;
// 					filenames += virgule_suite;
// 					for(var i = 0 ; i < files.length ; i++) {
// 						filenames += files[i].name + ", ";
// 						$(this).parent().next('td').append('<a class="draggable" target="_blank" href=../img/' + files[i].name + '><img src=../img/' + files[i].name + ' /></a>');
// 					}
// 				}

// 				filenames = filenames.substring(0,filenames.length-2);
// 				$(this).val(filenames);

// 				this.style.background = 'none';
// 			}, false);
// 		}

// 	};

// 	var draggables = document.querySelectorAll('.draggable');
// 	for (var i = 0 ; i < draggables.length ; i++) {
// 	    dndHandler.applyDragEvents(draggables[i]);
// 	}

// 	var droppers = document.querySelectorAll('.dropper');
// 	for (var i = 0 ; i < droppers.length ; i++) {
// 	    dndHandler.applyDropEvents(droppers[i]);
// 	}

});
