$(function(){
	$(window).load(function(){

		iframe_original_height = $('iframe').height();
		iframe_original_width = $('iframe').width();

		if (iframe_original_height < iframe_original_width) {
			// $('iframe').height($(window).height());
			// iframe_new_height = $('iframe').height();
			// $('iframe').width(iframe_new_height*iframe_original_width/iframe_original_height);
		}
		if (iframe_original_height > iframe_original_width) {
			// $('iframe').width('99%');
			// iframe_new_width = $('iframe').width();
			// $('iframe').height(iframe_new_width*iframe_original_height/iframe_original_width);
		}
		$('iframe').width('99%');
		$('iframe').height($(window).height());

		$('video').height($(window).height());

		$(window).resize(function(){
			if (iframe_original_height < iframe_original_width) {
				// $('iframe').height($(window).height());
				// iframe_new_height = $('iframe').height();
				// $('iframe').width(iframe_new_height*iframe_original_width/iframe_original_height);
			}
			if (iframe_original_height > iframe_original_width) {
				// $('iframe').width('99%');
				// iframe_new_width = $('iframe').width();
				// $('iframe').height(iframe_new_width*iframe_original_height/iframe_original_width);
			}

			$('video').height($(window).height());
		});


		// dépend des l'image
		var maxImgWidth, maxImgHeight;
		var nbImg;

		function size_and_position(){
			// indépendante
			navigationHeight = $('#navigation').height();

			maxImgWidth = 0;
			maxImgHeight = 0;
			nbImg = 0;

			if($(window).width() > 450) {
				// dépend de la page
				sliderHeight = $(window).height() - $('header').height();
				$('#slider').css('height', sliderHeight);

				// dépend des images
				$('#slider ul img').each(function(){
					$(this).css('height', sliderHeight - navigationHeight);

					if($(this).width() > maxImgWidth) {
						maxImgWidth = $(this).width();
					}
					nbImg++;
				});
				$('#slider').css('width', maxImgWidth);
				$('#slider li').css('width', maxImgWidth);
				$('#slider ul').css('width', nbImg*maxImgWidth);
		  }

		  if($(window).width() < 450) {
				// dépend de la page
				sliderWidth = $(window).width();
				$('#slider').width(sliderWidth);

				// dépend des images
				$('#slider ul img').each(function(){
					$(this).width(sliderWidth);

					if($(this).height() > maxImgHeight) {
						maxImgHeight = $(this).height();
					}
					nbImg++;
				});
				$('#slider').height(maxImgHeight + navigationHeight);
				$('#slider ul').width(nbImg*sliderWidth);
			}
		}
		size_and_position();
		$(window).resize(function(){
			size_and_position();
		});

		$(".illustration").click(function(){
			$(this).parent().prev('ul').animate({
				marginLeft: $(this).index() * -$('#slider li').width()
			});
		});

	});
});
