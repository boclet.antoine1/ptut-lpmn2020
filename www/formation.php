<?php $current = "Formation";
$title = "Formation | DUT MMI | IUT de Rouen, Elbeuf";
$description = "Site Internet du département MMI de l'IUT d'Elbeuf. Site réalisé dans le cadre des projets tuteurés, agence Colab 2017-2018. Ce DUT MMI vous permettra de poursuivre sur le site d'Elbeuf en licence professionnelle métiers du numérique conception réalisation rédaction WEB : LP MN CRR-WEB";

include_once('_inc/header.php'); ?>



<div class="enteteBoite">	
	<div class="titreBleu">La formation</div>
	<div class="titre">Que fait-on en MMI ?</div>
	<div class="boiteFlex">
		<div class="descFormation">	
			<p class="entete">Le DUT Métiers du Multimédia et de l'Internet (MMI) est une formation pluridisciplinaire dans le numérique.<br/><br/>De niveau bac +2, il forme des étudiants polyvalents dans les secteurs de la communication, du design, du développement web et de l'audiovisuel.</p>
		</div>
		<div class="forma">
			<div class="titre deb">Les débouchés</div>
			<div class="listMat">
				<div>
					<div>
						<img src="img/icons/web.svg" alt="">
						<div>Développeur Web</div>
					</div>
					<div>
						<img src="img/icons/video.svg" alt="">
						<div>Réalisateur Multimédia</div>
					</div>
					<div>
						<img src="img/icons/graphiste.svg" alt="">
						<div>Graphisme</div>
					</div>
					<div>
						<img src="img/icons/ux.svg" alt="">
						<div>UX Designer</div>
					</div>
				</div>
				<div>
					<div>
						<img src="img/icons/communityManager.svg" alt="">
						<div>Community Manager</div>
					</div>
					<div>
						<img src="img/icons/communication.svg" alt="">
						<div>Chargé de Communication</div>
					</div>
					<div>
						<img src="img/icons/video.svg" alt="">
						<div>Motion Designer</div>
					</div>
					<div>
						<img src="img/icons/chefProjet.svg" alt="">
						<div>Chef	 de Projet Multimédia</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="fontGris">
<article class="mat">
	<div data-aos="fade-up" data-aos-duration="750">
		<div id="lottieContainerWeb"></div>
		<div class="NomMat">Dévelopement Web</div>
		<div class="DescMat">Utiliser et concevoir une application web à travers des langages basiques. 
		<br />
		Apprendre à réaliser la mise en œuvre de tous les éléments visuels que les utilisateurs peuvent voir. 
		</div>
	</div>
	<div data-aos="fade-up" data-aos-duration="750" >
		<div id="lottieContainerCom"></div>
		<div class="NomMat">Communication</div>
		<div class="DescMat">Apprendre des stratégies marketing traditionnelles et spécifiques aux réseaux sociaux. 
		<br />
		Produire des contenus rédactionnels adaptés au web.</div>
	</div>
		<div data-aos="fade-up" data-aos-duration="750" class="cache">
			<div id="lottieContainerGraphisme"></div>
			<div class="NomMat">Graphisme</div>
			<div class="DescMat">Créer, choisir et utiliser des éléments graphiques avec les outils numériques.
			<br />
			Maîtriser Photoshop, Illustrator, InDesign et d’autres logiciels de graphisme.
			</div>
		</div>
		<div data-aos="fade-up" data-aos-duration="750" class="cache">
			<div id="lottieContainerConception"></div>
			<div class="NomMat">Conception artistique</div>
			<div class="DescMat">Transmettre une idée à travers une démarche artistique. Appliquer le théorique aux  éléments graphiques. 
			<br />
			Être capable d’établir une charte graphique.
			</div>
		</div>
		<div data-aos="fade-up" data-aos-duration="750" class="cache">
			<div id="lottieContainerAudio"></div>
			<div class="NomMat">Audiovisuel</div>
			<div class="DescMat">Cultiver ses connaissances en terme de vidéo et de son. Comprendre les métiers du montage vidéo et d'ingénieur son. 
			<br />
	 		Maîtriser les logiciels de montage comme Adobe Premiere Pro.</div>
		</div>
		<div data-aos="fade-up" data-aos-duration="750" class="cache">
			<div id="lottieContainerMotion"></div>
			<div class="NomMat">Animation 2D/3D</div>
			<div class="DescMat">Apprendre à utiliser les logiciels de motion design et de modélisation 3D tels que Blender ou After effect. 
			<br />
			Produire des modèles 3D, des court métrages d’animation et des effets spéciaux.
			</div>
		</div>
<button class='voirPlus'><div class="rond">+</div>Voir la suite</button>
<button class="buttonBleu center buttonForma" onclick="location.href='http://src-media.com/ppn-mmi/fiches-modules.html';" value="PPN">Voir le programme</button>
</article>
</div>
	<div class="numero">
			<div>
				<div>1800</div>
				<div>Heures</div>
			</div>
			<div>
				<div>300h</div>
				<div>de Projet</div>
			</div>
			<div>
				<div>4</div>
				<div>Pôles</div>
			</div>
			<div>
				<div>420h</div>
				<div>de Stages</div>
			</div>
		</div>	
		<p class="infos">Durant la deuxième année durant le S4, nos étudiants ont la possiblité de choisir entre la spécialisation Multimédia et Audiovisuelle, ce qui permet de se spécialiser dans leur domaine de prédilection.</p>
		<div class="fondBleu">
			<article class="flexPostul">
				<div class="postuler">Comment postuler ?</div>
				<div>
		       		 <p >
			           Le DUT MMI étant une formation publique, les candidatures se font sur Parcoursup.  <br/><br/>Le recrutement se fait selon le dossier, la motivation et les appétences personnelles du candidat.
			        </p>
			        <button class="buttonBlanc" onclick="location.href='https://www.parcoursup.fr/';" value="Parcoursup">Parcoursup</button>
				</div>
		    </article>
		</div>
		<?php include_once('_inc/footer.php');?>
