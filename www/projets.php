<?php $current = "Projets";
$title = "Projets | DUT MMI | IUT de Rouen, Elbeuf";
$description = "Site Internet du département MMI de l'IUT d'Elbeuf. Site réalisé dans le cadre des projets tuteurés, agence Colab 2017-2018. Ce DUT MMI vous permettra de poursuivre sur le site d'Elbeuf en licence professionnelle métiers du numérique conception réalisation rédaction WEB : LP MN CRR-WEB";

include_once('_inc/header.php'); ?>



<div class="boiteProjets">	
	<div class="titreBleu">Les projets</div>
	<article class="projets">
		<a href="jdm2019/index.html"><div>
			<h2>Journée des Métiers</h2>
			<p>Projet Tutoré 2019</p>
		</div></a>
		<a href="jeuvr2019/index.html"><div>
			<h2>Tiny Monkey (jeu VR)</h2>
			<p>Projet Tutoré 2019</p>			
		</div></a>
	</article>
</div>
<?php include_once('_inc/footer.php');?>
