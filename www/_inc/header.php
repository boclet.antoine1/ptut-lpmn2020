<?php include_once('_inc/config.php')  ?>

<!doctype html>
<html class="no-js" lang="fr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $title; ?></title>
  <meta name="Subject" content="DUT MMI Rouen">
  <meta name="Author" content="Des étudiants">
  <meta name="Identifier-Url" content="www.mmirouen.fr">
  <meta name="Revisit-After" content="10 days">
  <meta name="Robots" content="all">
  <meta name="Rating" content="general">
  <meta name="Distribution" content="global">
  <meta name="Geography" content="Rouen,France,Normandie">
  <meta name="Category" content="internet">
  <meta name="keywords" content="MMI, Numérique, formation dans le numérique, IUT ROUEN, IUT ELBEUF, DUT MMI, MMI ELBEUF, MMI ROUEN, Elbeuf, Rouen, Licence Professionnelle multimédia, licence professionnelle métiers du numérique, webdesigner, webdeveloppeur, LP MN, CRRW, WEB">
  <meta name="description" content="<?php echo $description; ?>">
  <link rel='icon' type="image/png" href="img/favicon.png">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
<header>
  <img onclick="window.location.href= ('<?php echo $route['home']; ?>')" src="img/logoMMI.svg">
  <nav>
    <a href="<?php echo $route['home']; ?>"><li>Accueil</li></a>
    <a href="<?php echo $route['formation']; ?>"><li>La formation</li></a>
    <a href="<?php echo $route['etudiants']; ?>"><li>Les étudiants</li></a>
    <a href="<?php echo $route['departement']; ?>"><li>Le département</li></a>
  </nav>  
  <div id="hamburger">
    <span></span>
    <span></span>
    <span></span>
  </div>
  <div id="menu-mobile">
    <div id="bg-menu-mobile">
    </div>
    <ul>
      <a href="<?php echo $route['home']; ?>"><li>Accueil</li></a>
      <a href="<?php echo $route['formation']; ?>"><li>La formation</li></a>
      <a href="<?php echo $route['etudiants']; ?>"><li>Les étudiants</li></a>
      <a href="<?php echo $route['departement']; ?>"><li>Le département</li></a>
    </ul>
  </div>
</header>
<main> 