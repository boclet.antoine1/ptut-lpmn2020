			</main>
		</body>
	<footer>
		<!-- MENU ACCORDEON -->
		<ul class="accordeon">
		    <li class="accordeon_item">
		        <h3 class="accordeon_title">Adresse</h3>
		        <div class="accordeon_content">
		            <p class="text">24 cours Gambetta</p>
		            <p class="text">76500 Elbeuf</p>
		            <p class="text">
		            	<a href="tel:+33232961038">02 32 96 10 38</a>
		            </p>
		            <p class="text">
		            	<a href="mailto:secretariat-mmi-iutrouen@univ-rouen.fr">secretariat-mmi-iutrouen@univ-rouen.fr</a>
		            </p>
		        </div>
		    </li>
		    <div class="trait"></div>	
		    <li class="accordeon_item">
		        <h3 class="accordeon_title">Liens utiles</h3>
		        <div class="accordeon_content">
		        	<p class="text">
		        		<a href="https://ent-wayf.normandie-univ.fr/">ENT</a>
		        	</p>
		            <p class="text">
		            	<a href="https://www.parcoursup.fr/">Admission</a>
		            </p>
		            <p class="text">
		            	<a href="https://webmail.ac-rouen.fr/">Webmail de Rouen</a>
		            </p>
		            <a href="http://www.iutrouen.univ-rouen.fr//">
		            	<img class="imgIut" src="img/iutrouen.svg">
		            </a>
		            <a href="http://www.univ-rouen.fr/">
		            	<img class="imgUniv" src="img/univRouen.svg">
		            </a>
		        </div>
		    </li>
		</ul>
		<!-- FIN MENU -->
		<!-- CONTENU POUR VERSION DESKTOP -->
		<div class="elmFooter">
			<div class="adresse">
				<a href="https://goo.gl/maps/6Mu9kEQGNHz32FN46">
				    <p class="text">24 cours Gambetta</p>
				    <p class="text">76500 Elbeuf</p>
				</a>
			    <p class="text">
			    	<a href="tel:+33232961038">02 32 96 10 38</a>
			    </p>
			    <p class="text">
			    	<a href="mailto:secretariat-mmi-iutrouen@univ-rouen.fr">secretariat-mmi-iutrouen@univ-rouen.fr</a>
			    </p>
			</div>
			<div class="liensUtiles">
				<p class="text"><a href="https://ent-wayf.normandie-univ.fr/">ENT</a></p>
				<p class="text"><a href="https://www.parcoursup.fr/">Admission</a></p>
			    <p class="text"><a href="https://webmail.ac-rouen.fr/">Webmail de Rouen</a></p>
			    <a href="http://www.iutrouen.univ-rouen.fr//">
			    	<img class="imgIut" src="img/iutrouen.svg">
			    </a>
			    <a href="http://www.univ-rouen.fr/">
			    	<img class="imgUniv" src="img/univRouen.svg">
			    </a>
			</div>
			<div class="iconsFooter">
				<a href="https://twitter.com/MMI_ROUEN">
					<img class="svgFooter" src="img/icons/twitter.svg">
				</a>
				<a href="https://www.youtube.com/channel/UCmemIDly8ZozE_q4-zg0wqg">
					<img class="svgFooter" src="img/icons/youtube.svg">
				</a>
				<a href="https://www.instagram.com/mmi_rouen/">
					<img class="svgFooter" src="img/icons/instagram.svg">
				</a>
			</div>
		</div>
		<!-- FIN -->
	</footer>
	</html>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.6.3/lottie.min.js"></script>
<!-- https://code.jquery.com/jquery-3.4.1.min.js -->
<script type="text/javascript" src="js/app.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
</body>
</html>
