<?php $current = "Département";
$title = "Département | DUT MMI | IUT de Rouen, Elbeuf";
$description = "Site Internet du département MMI de l'IUT d'Elbeuf. Site réalisé dans le cadre des projets tuteurés, agence Colab 2017-2018. Ce DUT MMI vous permettra de poursuivre sur le site d'Elbeuf en licence professionnelle métiers du numérique conception réalisation rédaction WEB : LP MN CRR-WEB";

include_once('_inc/header.php'); ?>  
<div class="enteteBoite">
    <div id="enteteDep">
        <h2 class="titreBleu">Le Département </h2>
        <div class="titre">L'équipe pédagogique</div>
        <p class="entete">L'IUT d'Elbeuf est construit sur le site des anciennes industries textiles. Il a été totalement réhabilité en 2002 afin d'y construire un nouveau bâtiment.</p>  
        <img src="img/IUT_bleu.svg" id="IUTbleu">
    </div>
</div>
<div class="dptm">   
    <article class="presentation">
        <div>
            <h1>24 Cours Gambetta<br/>
            76500 Elbeuf</h1>
        </div>
        <div>
            <p class="texteLocaux">Les locaux du département MMI comprennent un amphi de plus de 120 places, de nombreuses salles de cours, un studio adapté à l'enseignement de la vidéo et du son ainsi que des salles de Travaux Pratiques.<br/><br/> Elles sont dédiées à la programmation, l'intégration web, ou encore l'infographie.</p>
            <button class="buttonBleu" onclick="location.href='https://goo.gl/maps/PsSn8vt3yd1xWD6UA';" value="L'IUT sur Maps">Localiser l'IUT</button>
        </div>
    </article>
    <div class="numero num">
        <div>
            <div>3</div>
            <div>Casques VR</div>
        </div>
        <div>
            <div>45</div>
            <div>iMacs</div>
        </div>
        <div>
            <div>100</div>
            <div>PC</div>
        </div>
        <div>
            <div>1</div>
            <div>Studio</div>
        </div>
    </div>
    <section class="profs">
        <div class="row1">
            <div>
                <h3>Florian Joetzjer</h3>
                <p>Programmation Web et Dev</p><p class="gras">Directeur des études</p>
            </div>      
            <div>
                <h3>Sylvain Kerjean</h3>
                <p>Programmation et Réseaux</p><p class="gras">Responsable des études</p>
            </div>       
            <div>
                <h3>David Campserveux</h3>
                <p>Anglais</p><p class="gras">Responsable des stages</p>
            </div>
        </div>
        <div class="row2 cache">
            <div>
                <h3>Franck Beharelle</h3>
                <p>Graphisme</p>
            </div>   
            <div>
                <h3>Christophe Pique</h3>
                <p>Audiovisuel</p>
            </div>            
            <div>
                <h3>Jean-Loup Garulo</h3>
                <p>Communication Digitale/</p>
                <p>Gestion de Projet</p>
            </div>
        </div>
        <div class="row3 cache">
            <div>
                <h3>Jean-Pierre Lafitte</h3>
                <p>Economie / Droit</p>
            </div> 
            <div>
                <h3>Katerine Roméo</h3>
                <p>Sciences et Programmation</p>
            </div>
        </div>

        <button class='buttonBleu voirPlus'>Voir l'équipe pédagogique</button>
    </section>
    <p class="infos">L'équipe pédagogique est aussi composée d'intervenants extérieurs, étant tous des professionnels dans leur domaine. D'autres intervenants seront amenés à mener des conférences ou des ateliers durant les deux ans de formation.</p>
</div>
<?php include_once('_inc/footer.php') ?>